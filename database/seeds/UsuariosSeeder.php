<?php

use Illuminate\Database\Seeder;
use \App\Modules\Sisadm\Models;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = factory(\App\Modules\Sisadm\Models\User::class)->create([
            'no_usuario' => 'André Boaro',
            'email' => 'andre.boaro@fazenda.gov.br',
            'nr_cpf' => '70822921120',
            'password' => bcrypt(123456),
            'id_orgao' => 55,
            'sn_ldap' => false
        ]);

        $user2 = factory(\App\Modules\Sisadm\Models\User::class)->create([
            'no_usuario' => 'Luisa Palmeira',
            'email' => 'luisa.palmeira@fazenda.gov.br',
            'nr_cpf' => '03391664100',
            'password' => bcrypt(123456),
            'id_orgao' => 55,
            'sn_ldap' => false
        ]);

        $user3 = factory(\App\Modules\Sisadm\Models\User::class)->create([
            'no_usuario' => 'GLÁUCIA DE OLIVEIRA DIAS',
            'email' => 'glaucia.dias@fazenda.gov.br',
            'nr_cpf' => '76551121187',
            'password' => bcrypt(123456),
            'id_orgao' => 55,
            'sn_ldap' => false
        ]);
    }
}
