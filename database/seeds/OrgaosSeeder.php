<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Modules\Sisadm\Repositories\OrgaoRepository;

class OrgaosSeeder extends Seeder
{
    private $orgaoRepository;

    public function __construct(OrgaoRepository $orgaoRepository)
    {
        $this->orgaoRepository = $orgaoRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(OrgaoRepository $orgaoRepository)
    {

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "MF", 
            "no_orgao" => "MINISTERIO DA FAZENDA", 
            "id_municipio" => 756,
            "co_uorg" => "000050274", 
            "co_siafi" => 0, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
        ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "GMF", 
            "no_orgao" => "GABINETE DO MINISTRO DA FAZENDA", 
            "id_municipio" => 756,
            "co_uorg" => "000005534", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 1, 
        ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "AESP", 
            "no_orgao" => "ASSESSORIA ESPECIAL DO MINISTRO DE ESTADO", 
            "id_municipio" => 756,
            "co_uorg" => "000063035", 
            "co_siafi" => 170311, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 2, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "ASECOIN", 
            "no_orgao" => "ASSESSORIA ESPECIAL DE CONTROLE INTERNO", 
            "id_municipio" => 756,
            "co_uorg" => "000064044", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 2, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "COGER", 
            "no_orgao" => "CORREGEDORIA-GERAL", 
            "id_municipio" => 756,
            "co_uorg" => "000063459", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 3, 
            "id_orgao_id" => 2, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "STN", 
            "no_orgao" => "SECRETARIA DO TESOURO NACIONAL", 
            "id_municipio" => 756,
            "co_uorg" => "000005542", 
            "co_siafi" => 170500, 
            "sn_oficial" => 1, 
            "nr_ordem" => 4, 
            "id_orgao_id" => 1, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "ESAF", 
            "no_orgao" => "ESCOLA DE ADMINISTRACAO FAZENDARIA", 
            "id_municipio" => 756,
            "co_uorg" => "000005533", 
            "co_siafi" => 170009, 
            "sn_oficial" => 1, 
            "nr_ordem" => 5, 
            "id_orgao_id" => 1, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SE", 
            "no_orgao" => "SECRETARIA EXECUTIVA DO MINISTERIO DA FAZENDA", 
            "id_municipio" => 756,
            "co_uorg" => "000005535", 
            "co_siafi" => 170311, 
            "sn_oficial" => 1, 
            "nr_ordem" => 4, 
            "id_orgao_id" => 2, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "GABIN", 
            "no_orgao" => "GABINETE DA SECRETARIA EXECUTIVA", 
            "id_municipio" => 756,
            "co_uorg" => "000005525", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 8, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "CODIN", 
            "no_orgao" => "COORDENACAO DE DOCUMENTACAO E ASSUNTOS INSTITUCIONAIS", 
            "id_municipio" => 756,
            "co_uorg" => "000056203", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 9, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIDOC", 
            "no_orgao" => "DIVISAO DE COMUNICACAO ADMINISTRATIVA E DOCUMENTACAO", 
            "id_municipio" => 756,
            "co_uorg" => "000056204", 
            "co_siafi" => 0, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 10, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEPA", 
            "no_orgao" => "SERVICO DE PROTOCOLO E ARQUIVO", 
            "id_municipio" => 756,
            "co_uorg" => "000063632", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 11, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SERE", 
            "no_orgao" => "SERVICO DE REGISTRO E EXPEDICAO", 
            "id_municipio" => 756,
            "co_uorg" => "000063633", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 11, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIAA", 
            "no_orgao" => "DIVISAO DE ASSUNTOS ADMINISTRATIVOS", 
            "id_municipio" => 756,
            "co_uorg" => "000061980", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 10, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEAD", 
            "no_orgao" => "SERVICO DE APOIO ADMINISTRATIVO", 
            "id_municipio" => 756,
            "co_uorg" => "000063636", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 14, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "OUVIR", 
            "no_orgao" => "OUVIDORIA-GERAL", 
            "id_municipio" => 756,
            "co_uorg" => "000055053", 
            "co_siafi" => 17001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 8, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "GEOUVIR", 
            "no_orgao" => "GERENCIA DE OUVIDORIA", 
            "id_municipio" => 756,
            "co_uorg" => "000064048", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 16, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "GEOUVIR01", 
            "no_orgao" => "GERENCIA DE INFORMAÇÃO AO CIDADAO", 
            "id_municipio" => 756,
            "co_uorg" => "000064064", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 16, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEROUV", 
            "no_orgao" => "SERVICO DE APOIO INSTITUCIONAL", 
            "id_municipio" => 756,
            "co_uorg" => "000064049", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 3, 
            "id_orgao_id" => 16, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SUBGOES", 
            "no_orgao" => "SUBSECRETARIA DE GOVERNANCA DAS ESTATAIS", 
            "id_municipio" => 756,
            "co_uorg" => "000064050", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 3, 
            "id_orgao_id" => 8, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "COAGEST", 
            "no_orgao" => "COORDENACAO-GERAL DE AVALIACAO DA GOVERNANCA DAS  ESTATAIS", 
            "id_municipio" => 756,
            "co_uorg" => "000064051", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 20, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SPAE", 
            "no_orgao" => "SUBSECRETARIA PARA ASSUNTOS ECONOMICOS", 
            "id_municipio" => 756,
            "co_uorg" => "000056184", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 4, 
            "id_orgao_id" => 8, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SGE", 
            "no_orgao" => "SUBSECRETARIA DE GESTAO ESTRATEGICA", 
            "id_municipio" => 756,
            "co_uorg" => "000063030", 
            "co_siafi" => 170311, 
            "sn_oficial" => 1, 
            "nr_ordem" => 4, 
            "id_orgao_id" => 8, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "COINP", 
            "no_orgao" => "COORDENACAO-GERAL DE INOVACAO, PROJETOS E PROCESSOS", 
            "id_municipio" => 756,
            "co_uorg" => "000064053", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 23, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "CODIP", 
            "no_orgao" => "COORDENACAO-GERAL DE DESENVOLVIMENTO INSTITUCIONAL E PROGRAMAS DE GESTAO", 
            "id_municipio" => 756,
            "co_uorg" => "000063031", 
            "co_siafi" => 170311, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 23, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "COOPE", 
            "no_orgao" => "COORDENACAO-GERAL DE PROGRAMAS E PROJETOS DE COOPERACAO", 
            "id_municipio" => 756,
            "co_uorg" => "000063032", 
            "co_siafi" => 170311, 
            "sn_oficial" => 1, 
            "nr_ordem" => 3, 
            "id_orgao_id" => 23, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAIN", 
            "no_orgao" => "SECRETARIA DE ASSUNTOS INTERNACIONAIS", 
            "id_municipio" => 756,
            "co_uorg" => "000005770", 
            "co_siafi" => 170191, 
            "sn_oficial" => 1, 
            "nr_ordem" => 7, 
            "id_orgao_id" => 2, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SPE", 
            "no_orgao" => "SECRETARIA DE POLITICA ECONOMICA", 
            "id_municipio" => 756,
            "co_uorg" => "000005571", 
            "co_siafi" => 170250, 
            "sn_oficial" => 1, 
            "nr_ordem" => 8, 
            "id_orgao_id" => 2, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEPREV", 
            "no_orgao" => "SECRETARIA DE PREVIDENCIA DO MINISTERIO DA FAZENDA", 
            "id_municipio" => 756,
            "co_uorg" => "000064025", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 6, 
            "id_orgao_id" => 1, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "COAF", 
            "no_orgao" => "CONSELHO DE CONTROLE DE ATIVIDADES FINANCEIRAS", 
            "id_municipio" => 756,
            "co_uorg" => "000052264", 
            "co_siafi" => 170401, 
            "sn_oficial" => 1, 
            "nr_ordem" => 7, 
            "id_orgao_id" => 1, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "CARF", 
            "no_orgao" => "CONSELHO ADMINSTRATIVO DE RECURSOS FISCAIS", 
            "id_municipio" => 756,
            "co_uorg" => "000061640", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 8, 
            "id_orgao_id" => 1, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "CONFAZ", 
            "no_orgao" => "CONSELHO NACIONAL DE POLITICA FAZENDARIA", 
            "id_municipio" => 756,
            "co_uorg" => "000061639", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 9, 
            "id_orgao_id" => 1, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SPOA", 
            "no_orgao" => "SUBSECRETARIA DE PLANEJAMENTO, ORCAMENTO E ADMINISTRACAO", 
            "id_municipio" => 756,
            "co_uorg" => "000052906", 
            "co_siafi" => 170014, 
            "sn_oficial" => 1, 
            "nr_ordem" => 5, 
            "id_orgao_id" => 8, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "APOIO", 
            "no_orgao" => "SERVICO DE APOIO ADMINISTRATIVO", 
            "id_municipio" => 756,
            "co_uorg" => "000063071", 
            "co_siafi" => 170014, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "COGPL", 
            "no_orgao" => "COORDENACAO-GERAL DE PLANEJAMENTO E PROJETOS ORGANIZACIONAIS", 
            "id_municipio" => 756,
            "co_uorg" => "000057235", 
            "co_siafi" => 170014, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "CPLAN", 
            "no_orgao" => "COORDENACAO DE PLANEJAMENTO", 
            "id_municipio" => 756,
            "co_uorg" => "000055072", 
            "co_siafi" => 17001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 35, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "CPROJ", 
            "no_orgao" => "COORDENACAO DE PROJETOS E PROCESSOS ORGANIZACIONAIS", 
            "id_municipio" => 756,
            "co_uorg" => "000063072", 
            "co_siafi" => 170014, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 35, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "CCOM", 
            "no_orgao" => "COORDENACAO DE COMUNICACAO SOCIAL", 
            "id_municipio" => 756,
            "co_uorg" => "000064361", 
            "co_siafi" => 0, 
            "sn_oficial" => 1, 
            "nr_ordem" => 3, 
            "id_orgao_id" => 35, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "COGEF", 
            "no_orgao" => "COORDENACAO-GERAL DE ORCAMENTO E FINANCAS", 
            "id_municipio" => 756,
            "co_uorg" => "000055054", 
            "co_siafi" => 17001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 3, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "CPROR", 
            "no_orgao" => "COORDENACAO DE PROGRAMACAO ORCAMENTARIA", 
            "id_municipio" => 756,
            "co_uorg" => "000055073", 
            "co_siafi" => 17001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 39, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIVDE", 
            "no_orgao" => "DIVISAO DE CREDITOS DESCENTRALIZADOS", 
            "id_municipio" => 756,
            "co_uorg" => "000056214", 
            "co_siafi" => 17001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 40, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEPAC", 
            "no_orgao" => "SERVICO DE PROGRAMACAO, ACOMPANHAMENTO E AVALIACAODE CREDITOS DESCENTRALIZADOS", 
            "id_municipio" => 756,
            "co_uorg" => "000056215", 
            "co_siafi" => 17001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 41, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIVSU", 
            "no_orgao" => "DIVISAO DE CREDITOS SUPERVISIONADOS", 
            "id_municipio" => 756,
            "co_uorg" => "000056216", 
            "co_siafi" => 17001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 40, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIVDA", 
            "no_orgao" => "DIVISAO DE CREDITOS DA DIVIDA", 
            "id_municipio" => 756,
            "co_uorg" => "000056217", 
            "co_siafi" => 17001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 3, 
            "id_orgao_id" => 40, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "COPAF", 
            "no_orgao" => "COORDENACAO DE PROGRAMACAO DAS ATIVIDADES FINANCEIRAS", 
            "id_municipio" => 756,
            "co_uorg" => "000056218", 
            "co_siafi" => 170014, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 39, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIPRO", 
            "no_orgao" => "DIVISAO DE PROGRAMACAO FINANCEIRA", 
            "id_municipio" => 756,
            "co_uorg" => "000056219", 
            "co_siafi" => 170014, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 45, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "CGCON", 
            "no_orgao" => "COORDENACAO-GERAL DE CONTABILIDADE E CUSTOS", 
            "id_municipio" => 756,
            "co_uorg" => "000061483", 
            "co_siafi" => 17000, 
            "sn_oficial" => 1, 
            "nr_ordem" => 4, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "COGEP", 
            "no_orgao" => "COORDENACAO-GERAL DE GESTAO DE PESSOAS", 
            "id_municipio" => 756,
            "co_uorg" => "000062011", 
            "co_siafi" => 170006, 
            "sn_oficial" => 1, 
            "nr_ordem" => 5, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "CODEP", 
            "no_orgao" => "COORDENACAO DE DESENVOLVIMENTO DE PESSOAS", 
            "id_municipio" => 756,
            "co_uorg" => "000063074", 
            "co_siafi" => 170006, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 48, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIDEP", 
            "no_orgao" => "DIVISAO DE DESENVOLVIMENTO E RECRUTAMENTO DE PESSOAS", 
            "id_municipio" => 756,
            "co_uorg" => "000062016", 
            "co_siafi" => 170006, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 49, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "COARH", 
            "no_orgao" => "COORDENACAO DE ADMINISTRACAO DE RECURSOS HUMANOS", 
            "id_municipio" => 756,
            "co_uorg" => "000063077", 
            "co_siafi" => 170006, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 48, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "COGTI", 
            "no_orgao" => "COORDENACAO-GERAL DE TECNOLOGIA DA INFORMACAO", 
            "id_municipio" => 756,
            "co_uorg" => "000057236", 
            "co_siafi" => 170014, 
            "sn_oficial" => 1, 
            "nr_ordem" => 5, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "CGSER", 
            "no_orgao" => "COORDENACAO DE GESTAO DE SERVICOS DE TI", 
            "id_municipio" => 756,
            "co_uorg" => "000056233", 
            "co_siafi" => 170014, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 52, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "CGEST", 
            "no_orgao" => "COORDENACAO DE GESTAO DE ESTRATEGIAS DE TI", 
            "id_municipio" => 756,
            "co_uorg" => "000064065", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 52, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DISAQ", 
            "no_orgao" => "DIVISAO DE SOLUCAO E ARQUITETURA EM TI", 
            "id_municipio" => 756,
            "co_uorg" => "000056234", 
            "co_siafi" => 170014, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 53, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SESAT", 
            "no_orgao" => "SERVICO DE SUPORTE E ATENDIMENTO DE TI", 
            "id_municipio" => 756,
            "co_uorg" => "000056235", 
            "co_siafi" => 170014, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 53, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEPTI", 
            "no_orgao" => "SERVICO DE PLANEJAMENTO, CONFORMIDADE E RISCO EM TI", 
            "id_municipio" => 756,
            "co_uorg" => "000064069", 
            "co_siafi" => 170001, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 54, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "COGRL", 
            "no_orgao" => "COORDENACAO-GERAL DE RECURSOS LOGISTICOS", 
            "id_municipio" => 756,
            "co_uorg" => "000052911", 
            "co_siafi" => 170016, 
            "sn_oficial" => 1, 
            "nr_ordem" => 6, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "COLOG", 
            "no_orgao" => "COORDENACAO DE LOGISTICA", 
            "id_municipio" => 756,
            "co_uorg" => "000063084", 
            "co_siafi" => 170016, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 58, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DILOG", 
            "no_orgao" => "DIVISAO DE LOGISTICA", 
            "id_municipio" => 756,
            "co_uorg" => "000063085", 
            "co_siafi" => 170016, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 59, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "CODOC", 
            "no_orgao" => "COORDENACAO DE DOCUMENTACAO E INFORMACAO", 
            "id_municipio" => 756,
            "co_uorg" => "000063088", 
            "co_siafi" => 170016, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 58, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIDOC", 
            "no_orgao" => "DIVISAO DE DOCUMENTACAO E INFORMACAO", 
            "id_municipio" => 756,
            "co_uorg" => "000063089", 
            "co_siafi" => 170016, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 61, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEDOC", 
            "no_orgao" => "SERVICO DE DOCUMENTACAO E INFORMACAO", 
            "id_municipio" => 756,
            "co_uorg" => "000063090", 
            "co_siafi" => 170016, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 62, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "NUCLEO-AC", 
            "no_orgao" => "NUCLEO DE TRABALHO ESTADO DO ACRE", 
            "id_municipio" => 4194,
            "co_uorg" => "000063669", 
            "co_siafi" => 170344, 
            "sn_oficial" => 1, 
            "nr_ordem" => 7, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIV", 
            "no_orgao" => "DIVISAO DO NUCLEO DE TRABALHO", 
            "id_municipio" => 4194,
            "co_uorg" => "000063701", 
            "co_siafi" => 170344, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 64, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIGEP", 
            "no_orgao" => "DIVISAO DE GESTAO DE PESSOAS", 
            "id_municipio" => 4194,
            "co_uorg" => "000063702", 
            "co_siafi" => 170344, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 64, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "NUCLEO-AP", 
            "no_orgao" => "NUCLEO DE TRABALHO ESTADO DO AMAPA", 
            "id_municipio" => 2832,
            "co_uorg" => "000063670", 
            "co_siafi" => 170345, 
            "sn_oficial" => 1, 
            "nr_ordem" => 8, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIV", 
            "no_orgao" => "DIVISAO DO NUCLEO DE TRABALHO", 
            "id_municipio" => 2832,
            "co_uorg" => "000063699", 
            "co_siafi" => 170345, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 67, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIGEP", 
            "no_orgao" => "DIVISAO DE GESTAO DE PESSOAS", 
            "id_municipio" => 2832,
            "co_uorg" => "000063700", 
            "co_siafi" => 170345, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 67, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "NUCLEO-RO", 
            "no_orgao" => "NUCLEO DE TRABALHO ESTADO RONDONIA", 
            "id_municipio" => 3978,
            "co_uorg" => "000063671", 
            "co_siafi" => 170346, 
            "sn_oficial" => 1, 
            "nr_ordem" => 9, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIV", 
            "no_orgao" => "DIVISAO DO NUCLEO DE TRABALHO", 
            "id_municipio" => 3978,
            "co_uorg" => "000063703", 
            "co_siafi" => 170346, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 70, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIGEP", 
            "no_orgao" => "DIVISAO DE GESTAO DE PESSOAS", 
            "id_municipio" => 3978,
            "co_uorg" => "000063704", 
            "co_siafi" => 170346, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 70, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "NUCLEO-RR", 
            "no_orgao" => "NUCLEO DE TRABALHO ESTADO RORAIMA", 
            "id_municipio" => 643,
            "co_uorg" => "000063672", 
            "co_siafi" => 170347, 
            "sn_oficial" => 1, 
            "nr_ordem" => 10, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIV", 
            "no_orgao" => "DIVISAO DO NUCLEO DE TRABALHO", 
            "id_municipio" => 643,
            "co_uorg" => "000063705", 
            "co_siafi" => 170347, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 73, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIGEP", 
            "no_orgao" => "DIVISAO DE GESTAO DE PESSOAS", 
            "id_municipio" => 643,
            "co_uorg" => "000063706", 
            "co_siafi" => 170347, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 73, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-AL", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DE ALAGOAS", 
            "id_municipio" => 2840,
            "co_uorg" => "000061982", 
            "co_siafi" => 170064, 
            "sn_oficial" => 1, 
            "nr_ordem" => 11, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-AM", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DO AMAZONAS", 
            "id_municipio" => 2886,
            "co_uorg" => "000062001", 
            "co_siafi" => 170207, 
            "sn_oficial" => 1, 
            "nr_ordem" => 12, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DRL", 
            "no_orgao" => "DIVISAO DE RECURSOS LOGISTICOS", 
            "id_municipio" => 2886,
            "co_uorg" => "000063168", 
            "co_siafi" => 170217, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 77, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SISUP", 
            "no_orgao" => "SERVICO DE SUPRIMENTOS", 
            "id_municipio" => 2886,
            "co_uorg" => "000063169", 
            "co_siafi" => 170207, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 78, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIGEP", 
            "no_orgao" => "DIVISAO DE GESTAO DE PESSOAS", 
            "id_municipio" => 2886,
            "co_uorg" => "000063170", 
            "co_siafi" => 170207, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 77, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEATI", 
            "no_orgao" => "SERVICO DE ATIVOS DA DIVISAO DE GESTAO DE PESSOAS DA SUP. DE ADMINISTRACAO DO MIN. DA FAZENDA/AM", 
            "id_municipio" => 2886,
            "co_uorg" => "000063171", 
            "co_siafi" => 170207, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 80, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-BA", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DA BAHIA", 
            "id_municipio" => 4334,
            "co_uorg" => "000061999", 
            "co_siafi" => 170075, 
            "sn_oficial" => 1, 
            "nr_ordem" => 14, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DRL", 
            "no_orgao" => "DIVISAO DE RECURSOS LOGISTICOS", 
            "id_municipio" => 4334,
            "co_uorg" => "000063112", 
            "co_siafi" => 170075, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 82, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SISUP", 
            "no_orgao" => "SERVICO DE SUPRIMENTOS", 
            "id_municipio" => 4334,
            "co_uorg" => "000063113", 
            "co_siafi" => 170075, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 83, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIGEP", 
            "no_orgao" => "DIVISAO DE GESTAO DE PESSOAS", 
            "id_municipio" => 4334,
            "co_uorg" => "000063114", 
            "co_siafi" => 170075, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 82, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEATI", 
            "no_orgao" => "SERVICO DE ATIVOS", 
            "id_municipio" => 4334,
            "co_uorg" => "000063115", 
            "co_siafi" => 170075, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 85, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SINPE", 
            "no_orgao" => "SERVICO DE INATIVOS", 
            "id_municipio" => 4334,
            "co_uorg" => "000063116", 
            "co_siafi" => 170075, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 85, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-CE", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DO CEARA", 
            "id_municipio" => 1832,
            "co_uorg" => "000061998", 
            "co_siafi" => 170038, 
            "sn_oficial" => 1, 
            "nr_ordem" => 15, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DRL", 
            "no_orgao" => "DIVISAO DE RECURSOS LOGISTICOS", 
            "id_municipio" => 1832,
            "co_uorg" => "000063119", 
            "co_siafi" => 170038, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 88, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SISUP", 
            "no_orgao" => "SERVICO DE SUPRIMENTOS", 
            "id_municipio" => 1832,
            "co_uorg" => "000063120", 
            "co_siafi" => 170038, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 89, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIGEP", 
            "no_orgao" => "DIVISAO DE GESTAO DE PESSOAS", 
            "id_municipio" => 1832,
            "co_uorg" => "000063121", 
            "co_siafi" => 170038, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 88, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEATI", 
            "no_orgao" => "SERVICO DE ATIVOS", 
            "id_municipio" => 1832,
            "co_uorg" => "000063122", 
            "co_siafi" => 170038, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 91, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SINPE", 
            "no_orgao" => "SERVICO DE INATIVOS E PENSIONISTAS", 
            "id_municipio" => 1832,
            "co_uorg" => "000063123", 
            "co_siafi" => 170038, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 91, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-DF", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO DISTRITO FEDERAL", 
            "id_municipio" => 756,
            "co_uorg" => "000061981", 
            "co_siafi" => 170531, 
            "sn_oficial" => 1, 
            "nr_ordem" => 16, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "GEOFI", 
            "no_orgao" => "GERENCIA DE PLANEJAMENTO, ORCAMENTO E FINANCAS", 
            "id_municipio" => 756,
            "co_uorg" => "000063092", 
            "co_siafi" => 170531, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 94, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIOFI", 
            "no_orgao" => "DIVISAO DE ORCAMENTO E FINANCAS", 
            "id_municipio" => 756,
            "co_uorg" => "000063093", 
            "co_siafi" => 170531, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 95, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEOFI", 
            "no_orgao" => "SERVICO DE ORCAMENTO E FINANCAS", 
            "id_municipio" => 756,
            "co_uorg" => "000063094", 
            "co_siafi" => 170531, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 95, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "GELOG", 
            "no_orgao" => "GERENCIA DE RECURSOS LOGISTICOS", 
            "id_municipio" => 756,
            "co_uorg" => "000063095", 
            "co_siafi" => 170531, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 94, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DISUP", 
            "no_orgao" => "DIVISAO DE SUPRIMENTOS", 
            "id_municipio" => 756,
            "co_uorg" => "000063096", 
            "co_siafi" => 170531, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 98, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIFRA", 
            "no_orgao" => "DIVISAO DE INFRAESTRUTURA", 
            "id_municipio" => 756,
            "co_uorg" => "000063097", 
            "co_siafi" => 170531, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 98, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "GESPE", 
            "no_orgao" => "GERENCIA DE GESTAO DE PESSOAS", 
            "id_municipio" => 756,
            "co_uorg" => "000063099", 
            "co_siafi" => 170531, 
            "sn_oficial" => 1, 
            "nr_ordem" => 3, 
            "id_orgao_id" => 94, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIARH", 
            "no_orgao" => "DIVISAO DE ADMINISTRACAO DE RECURSOS HUMANOS", 
            "id_municipio" => 756,
            "co_uorg" => "000063100", 
            "co_siafi" => 170531, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 101, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEATI", 
            "no_orgao" => "SERVICO DE ATIVOS", 
            "id_municipio" => 756,
            "co_uorg" => "000063101", 
            "co_siafi" => 170531, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 101, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SINPE", 
            "no_orgao" => "SERVICO DE INATIVOS E PENSIONISTAS", 
            "id_municipio" => 756,
            "co_uorg" => "000063102", 
            "co_siafi" => 170531, 
            "sn_oficial" => 1, 
            "nr_ordem" => 3, 
            "id_orgao_id" => 101, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-ES", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DO ESPIRITO SANTO", 
            "id_municipio" => 5534,
            "co_uorg" => "000061983", 
            "co_siafi" => 170100, 
            "sn_oficial" => 1, 
            "nr_ordem" => 17, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-GO", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DE GOIAS", 
            "id_municipio" => 1928,
            "co_uorg" => "000061987", 
            "co_siafi" => 170195, 
            "sn_oficial" => 1, 
            "nr_ordem" => 18, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-MA", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DO MARANHAO", 
            "id_municipio" => 4812,
            "co_uorg" => "000061988", 
            "co_siafi" => 170025, 
            "sn_oficial" => 1, 
            "nr_ordem" => 19, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-MG", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DE MINAS GERAIS", 
            "id_municipio" => 593,
            "co_uorg" => "000061985", 
            "co_siafi" => 170014, 
            "sn_oficial" => 1, 
            "nr_ordem" => 20, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DRL", 
            "no_orgao" => "DIVISAO DE RECURSOS LOGISTICOS", 
            "id_municipio" => 593,
            "co_uorg" => "000063126", 
            "co_siafi" => 170014, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 108, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SISUP", 
            "no_orgao" => "SERVICO DE SUPRIMENTOS", 
            "id_municipio" => 593,
            "co_uorg" => "000063127", 
            "co_siafi" => 170014, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 109, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIGEP", 
            "no_orgao" => "DIVISAO DE GESTAO DE PESSOAS", 
            "id_municipio" => 593,
            "co_uorg" => "000063128", 
            "co_siafi" => 170014, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 108, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEATI", 
            "no_orgao" => "SERVICO DE ATIVOS", 
            "id_municipio" => 593,
            "co_uorg" => "000063129", 
            "co_siafi" => 170014, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 111, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SINPE", 
            "no_orgao" => "SERVICO DE INATIVOS E PENSIONISTAS", 
            "id_municipio" => 593,
            "co_uorg" => "000063130", 
            "co_siafi" => 170014, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 111, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-MS", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DE MATO GROSSO DO SUL", 
            "id_municipio" => 973,
            "co_uorg" => "000061989", 
            "co_siafi" => 170106, 
            "sn_oficial" => 1, 
            "nr_ordem" => 21, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-MT", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DO MATO GROSSO", 
            "id_municipio" => 1492,
            "co_uorg" => "000062002", 
            "co_siafi" => 170190, 
            "sn_oficial" => 1, 
            "nr_ordem" => 22, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DRL", 
            "no_orgao" => "DIVISAO DE RECURSOS LOGISTICOS", 
            "id_municipio" => 1492,
            "co_uorg" => "000063174", 
            "co_siafi" => 170190, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 115, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SISUP", 
            "no_orgao" => "SERVICO DE SUPRIMENTOS", 
            "id_municipio" => 1492,
            "co_uorg" => "000063175", 
            "co_siafi" => 170190, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 116, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIGEP", 
            "no_orgao" => "DIVISAO DE GESTAO DE PESSOAS", 
            "id_municipio" => 1492,
            "co_uorg" => "000063176", 
            "co_siafi" => 170190, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 115, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEATI", 
            "no_orgao" => "SERVICO DE ATIVOS", 
            "id_municipio" => 1492,
            "co_uorg" => "000063177", 
            "co_siafi" => 170190, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 118, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-PA", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DO PARA", 
            "id_municipio" => 581,
            "co_uorg" => "000062000", 
            "co_siafi" => 170214, 
            "sn_oficial" => 1, 
            "nr_ordem" => 23, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DRL", 
            "no_orgao" => "DIVISAO DE RECURSOS LOGISTICOS", 
            "id_municipio" => 581,
            "co_uorg" => "000063133", 
            "co_siafi" => 170214, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 120, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SISUP", 
            "no_orgao" => "SERVICO DE SUPRIMENTOS", 
            "id_municipio" => 581,
            "co_uorg" => "000063134", 
            "co_siafi" => 170214, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 121, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIGEP", 
            "no_orgao" => "DIVISAO DE GESTAO DE PESSOAS", 
            "id_municipio" => 581,
            "co_uorg" => "000063135", 
            "co_siafi" => 170214, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 120, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEATI", 
            "no_orgao" => "SERVICO DE ATIVOS", 
            "id_municipio" => 581,
            "co_uorg" => "000063136", 
            "co_siafi" => 170214, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 123, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SINPE", 
            "no_orgao" => "SERVICO DE INATIVOS E PENSIONISTAS", 
            "id_municipio" => 581,
            "co_uorg" => "000063137", 
            "co_siafi" => 170214, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 123, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-PB", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DA PARAIBA", 
            "id_municipio" => 2596,
            "co_uorg" => "000061990", 
            "co_siafi" => 170050, 
            "sn_oficial" => 1, 
            "nr_ordem" => 24, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-PE", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DE PERNAMBUCO", 
            "id_municipio" => 4112,
            "co_uorg" => "000061986", 
            "co_siafi" => 170055, 
            "sn_oficial" => 1, 
            "nr_ordem" => 25, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DRL", 
            "no_orgao" => "DIVISAO DE RECURSOS LOGISTICOS", 
            "id_municipio" => 4112,
            "co_uorg" => "000063147", 
            "co_siafi" => 170055, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 127, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SISUP", 
            "no_orgao" => "SERVICO DE SUPRIMENTOS", 
            "id_municipio" => 4112,
            "co_uorg" => "000063148", 
            "co_siafi" => 170055, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 128, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIGEP", 
            "no_orgao" => "DIVISAO DE GESTAO DE PESSOAS", 
            "id_municipio" => 4112,
            "co_uorg" => "000063149", 
            "co_siafi" => 170055, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 127, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEATI", 
            "no_orgao" => "SERVICO DE ATIVOS", 
            "id_municipio" => 4112,
            "co_uorg" => "000063150", 
            "co_siafi" => 170055, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 130, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SINPE", 
            "no_orgao" => "SERVICO DE INATIVOS E PENSIONISTAS", 
            "id_municipio" => 4112,
            "co_uorg" => "000063151", 
            "co_siafi" => 170055, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 130, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-PI", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DO PIAUI", 
            "id_municipio" => 5210,
            "co_uorg" => "000061991", 
            "co_siafi" => 170032, 
            "sn_oficial" => 1, 
            "nr_ordem" => 26, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-PR", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DO PARANA", 
            "id_municipio" => 1509,
            "co_uorg" => "000061995", 
            "co_siafi" => 170153, 
            "sn_oficial" => 1, 
            "nr_ordem" => 27, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DRL", 
            "no_orgao" => "DIVISAO DE RECURSOS LOGISTICOS", 
            "id_municipio" => 1509,
            "co_uorg" => "000063140", 
            "co_siafi" => 170153, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 134, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SISUP", 
            "no_orgao" => "SERVICO DE SUPRIMENTOS", 
            "id_municipio" => 1509,
            "co_uorg" => "000063141", 
            "co_siafi" => 170153, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 135, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIGEP", 
            "no_orgao" => "DIVISAO DE GESTAO DE PESSOAS", 
            "id_municipio" => 1509,
            "co_uorg" => "000063142", 
            "co_siafi" => 170153, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 134, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEATI", 
            "no_orgao" => "SERVICO DE ATIVOS", 
            "id_municipio" => 1509,
            "co_uorg" => "000063143", 
            "co_siafi" => 170153, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 137, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SINPE", 
            "no_orgao" => "SERVICO DE INATIVOS E PENSIONISTAS", 
            "id_municipio" => 1509,
            "co_uorg" => "000063144", 
            "co_siafi" => 170153, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 137, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-RJ", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DO RIO DE JANEIRO", 
            "id_municipio" => 4209,
            "co_uorg" => "000061984", 
            "co_siafi" => 170114, 
            "sn_oficial" => 1, 
            "nr_ordem" => 28, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "GRL", 
            "no_orgao" => "GERENCIA DE RECURSOS LOGISTICOS", 
            "id_municipio" => 4209,
            "co_uorg" => "000063105", 
            "co_siafi" => 170114, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 140, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SISUP", 
            "no_orgao" => "SERVICO DE SUPRIMENTOS", 
            "id_municipio" => 4209,
            "co_uorg" => "000063106", 
            "co_siafi" => 170114, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 141, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "GESPE", 
            "no_orgao" => "GERENCIA DE GESTAO DE PESSOAS", 
            "id_municipio" => 4209,
            "co_uorg" => "000063107", 
            "co_siafi" => 170114, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 140, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEATI", 
            "no_orgao" => "SERVICO DE ATIVOS", 
            "id_municipio" => 4209,
            "co_uorg" => "000063108", 
            "co_siafi" => 170114, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 143, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SINPE", 
            "no_orgao" => "SERVICO DE INATIVOS E PENSIONISTAS", 
            "id_municipio" => 4209,
            "co_uorg" => "000063109", 
            "co_siafi" => 170114, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 143, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-RN", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DO RIO GRANDE DO NORTE", 
            "id_municipio" => 3240,
            "co_uorg" => "000061992", 
            "co_siafi" => 170045, 
            "sn_oficial" => 1, 
            "nr_ordem" => 29, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-RS", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DO RIO GRANDE DO SUL", 
            "id_municipio" => 3948,
            "co_uorg" => "000061996", 
            "co_siafi" => 170175, 
            "sn_oficial" => 1, 
            "nr_ordem" => 30, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DRL", 
            "no_orgao" => "DIVISAO DE RECURSOS LOGISTICOS", 
            "id_municipio" => 3948,
            "co_uorg" => "000063154", 
            "co_siafi" => 170175, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 147, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SISUP", 
            "no_orgao" => "SERVICO DE SUPRIMENTOS", 
            "id_municipio" => 3948,
            "co_uorg" => "000063155", 
            "co_siafi" => 170175, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 148, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIGEP", 
            "no_orgao" => "DIVISAO DE GESTAO DE PESSOAS", 
            "id_municipio" => 3948,
            "co_uorg" => "000063156", 
            "co_siafi" => 170175, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 147, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEATI", 
            "no_orgao" => "SERVICO DE ATIVOS", 
            "id_municipio" => 3948,
            "co_uorg" => "000063157", 
            "co_siafi" => 170175, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 150, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SINPE", 
            "no_orgao" => "SERVICO DE INATIVOS E PENSIONISTAS", 
            "id_municipio" => 3948,
            "co_uorg" => "000063158", 
            "co_siafi" => 170175, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 150, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-SC", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DE SANTA CATARINA", 
            "id_municipio" => 1813,
            "co_uorg" => "000061993", 
            "co_siafi" => 170166, 
            "sn_oficial" => 1, 
            "nr_ordem" => 31, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-SE", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DE SERGIPE", 
            "id_municipio" => 295,
            "co_uorg" => "000061994", 
            "co_siafi" => 170069, 
            "sn_oficial" => 1, 
            "nr_ordem" => 32, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SAMF-SP", 
            "no_orgao" => "SUPERINTENDENCIA DE ADMINISTRACAO DO MF NO ESTADO DE SAO PAULO", 
            "id_municipio" => 4855,
            "co_uorg" => "000061997", 
            "co_siafi" => 170131, 
            "sn_oficial" => 1, 
            "nr_ordem" => 33, 
            "id_orgao_id" => 33, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DRL", 
            "no_orgao" => "DIVISAO DE RECURSOS LOGISTICOS", 
            "id_municipio" => 4855,
            "co_uorg" => "000063161", 
            "co_siafi" => 170131, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 155, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SISUP", 
            "no_orgao" => "SERVICO DE SUPRIMENTOS", 
            "id_municipio" => 4855,
            "co_uorg" => "000063162", 
            "co_siafi" => 170131, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 156, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "DIGEP", 
            "no_orgao" => "DIVISAO DE GESTAO DE PESSOAS", 
            "id_municipio" => 4855,
            "co_uorg" => "000063163", 
            "co_siafi" => 170131, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 155, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SEATI", 
            "no_orgao" => "SERVICO DE ATIVOS", 
            "id_municipio" => 4855,
            "co_uorg" => "000063164", 
            "co_siafi" => 170131, 
            "sn_oficial" => 1, 
            "nr_ordem" => 1, 
            "id_orgao_id" => 158, 
            ]);  

        $this->orgaoRepository->firstOrCreate([ 
            "sg_orgao" => "SINPE", 
            "no_orgao" => "SERVICO DE INATIVOS E PENSIONISTAS", 
            "id_municipio" => 4855,
            "co_uorg" => "000063165", 
            "co_siafi" => 170131, 
            "sn_oficial" => 1, 
            "nr_ordem" => 2, 
            "id_orgao_id" => 158, 
            ]);  


        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "RFB", "no_orgao" => "SECRETARIA DA RECEITA FEDERAL",
            "id_municipio" => 756,
            "co_uorg" => "000057360",
            "co_siafi" => 170010,
            "sn_oficial" => 1,
            "nr_ordem" => 10,
            "id_orgao_id" => 1,
        ]);        

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "PGFN",
            "no_orgao" => "PROCURADORIA-GERAL DA FAZENDA NACIONAL",
            "id_municipio" => 756,
            "co_uorg" => "000005636",
            "co_siafi" => 170008,
            "sn_oficial" => 1,
            "nr_ordem" => 11,
            "id_orgao_id" => 1,
        ]);  



        //OUTROS ORGAOS FORA DO MF
        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "CEF",
            "no_orgao" => "CAIXA ECONOMICA FEDERAL",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 7,
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "BCB",
            "no_orgao" => "BANCO CENTRAL DO BRASIL",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 4,    
        ]); 

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "BB",
            "no_orgao" => "BANCO DO BRASIL",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 3, 
        ]); 

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SUSEP",
            "no_orgao" => "SUPERINTENDENCIA DE SEGUROS PRIVADOS",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 13, 
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "CVM",
            "no_orgao" => "COMISSAO DE VALORES MOBILIARIOS",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 8,
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "CGSN",
            "no_orgao" => "CONSELHO GESTOR DO SIMPLES NACIONAL",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 8,
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "PREVIC",
            "no_orgao" => "SUPERINTENDENCIA NACIONAL DE PREVIDENCIA COMPLEMENTAR",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 14,
        ]); 

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "PR",
            "no_orgao" => "PRESIDENCIA DA REPUBLICA",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 15,
        ]); 

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "CD",
            "no_orgao" => "CÂMARA DOS DEPUTADOS",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 15,
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SF",
            "no_orgao" => "SENADO FEDERAL",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 15,
        
        ]);

        //ORGAOS EXCLUIDOS PARA MIGRACAO DO PARLA
        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "DR. LÍSCIO",
            "no_orgao" => "DR. LÍSCIO",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 2,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "BASA",
            "no_orgao" => "BANCO DA AMAZONIA",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 2,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "BMB",
            "no_orgao" => "BANCO DA AMAZONIA",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 2,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "BNB",
            "no_orgao" => "BANCO DO NORDESTE",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 5,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "BNDES",
            "no_orgao" => "BANCO NACIONAL DO DESENVOLVIMENTO",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 5,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "CEST",
            "no_orgao" => "CEST",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 5,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "CFP",
            "no_orgao" => "CFP",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 5,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "CISET",
            "no_orgao" => "CISET",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 5,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "CMB",
            "no_orgao" => "CASA DA MOEDA DO BRASIL",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "COTEPE",
            "no_orgao" => "COMISSÃO TÉCNICA PERMANENTE DO ICMS",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "CMN",
            "no_orgao" => "CONSELHO MONETÁRIO NACIONAL",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "CRH",
            "no_orgao" => "CRH",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "DAIN",
            "no_orgao" => "DAIN",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "DAP",
            "no_orgao" => "DAP",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "DEAIN",
            "no_orgao" => "DEAIN",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "DECEX",
            "no_orgao" => "DECEX",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "DIC",
            "no_orgao" => "DIC",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "DNPA",
            "no_orgao" => "DNPA",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "DOU",
            "no_orgao" => "DOU",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "DPU",
            "no_orgao" => "DPU",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "DRF",
            "no_orgao" => "DRF",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "DRJ",
            "no_orgao" => "DRJ",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "DTN",
            "no_orgao" => "DTN",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "IBGE",
            "no_orgao" => "IBGE",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "IRB",
            "no_orgao" => "INSTITUTO DE RESSEGUROS DO BRASIL",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 10,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SAA",
            "no_orgao" => "SAA",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SAG",
            "no_orgao" => "SAG",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "EXTERNO",
            "no_orgao" => "EXTERNO",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SEPE",
            "no_orgao" => "SEPE",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SEPLAN",
            "no_orgao" => "SEPLAN",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SFC",
            "no_orgao" => "SFC",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SFN",
            "no_orgao" => "SFN",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SNE",
            "no_orgao" => "SNE",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SNP",
            "no_orgao" => "SNP",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "AVISO CCIV",
            "no_orgao" => "AVISO CCIV",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SPU",
            "no_orgao" => "SPU",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SUNAB",
            "no_orgao" => "SUNAB",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SERPRO",
            "no_orgao" => "SERVICO FEDERAL DE PROCESSAMENTO DE DADOS",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 11,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "CONJUR",
            "no_orgao" => "CONJUR",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "IBS",
            "no_orgao" => "IBS",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "MPAS",
            "no_orgao" => "MPAS",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "GM",
            "no_orgao" => "GM",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SE/COTEPE",
            "no_orgao" => "SE/COTEPE",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "DR.GERARDO",
            "no_orgao" => "DR.GERARDO",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "AVISO PR",
            "no_orgao" => "AVISO PR",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "EMGEA",
            "no_orgao" => "EMPRESA GESTORA DE ATIVOS",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 9,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "OFC/PR",
            "no_orgao" => "OFC/PR",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "PR/OFC",
            "no_orgao" => "PR/OFC",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SRP",
            "no_orgao" => "SRP",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SEP",
            "no_orgao" => "SEP",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "OFI/AAP",
            "no_orgao" => "OFI/AAP",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "OFC/SE",
            "no_orgao" => "OFC/SE",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "OFC/SF",
            "no_orgao" => "OFC/SF",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SUPAR/SRI",
            "no_orgao" => "SUPAR/SRI",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "OFI/SRI",
            "no_orgao" => "OFI/SRI",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "AVI/MF",
            "no_orgao" => "AVI/MF",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "CCP",
            "no_orgao" => "CCP",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "DR EDMUNDO",
            "no_orgao" => "DR EDMUNDO",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SEREF",
            "no_orgao" => "SEREF",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 12,
            "deleted_at" => Carbon::now()            
        ]); 

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "SPPC",
            "no_orgao" => "SPPC",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "MPS",
            "no_orgao" => "MPS",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "PREVIDENCI",
            "no_orgao" => "PREVIDENCI",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        $this->orgaoRepository->firstOrCreate([
            "sg_orgao" => "NÃO INFORMADO",
            "no_orgao" => "NÃO INFORMADO",
            "id_municipio" => 756,
            "sn_oficial" => 0,
            "nr_ordem" => 6,
            "deleted_at" => Carbon::now()
        ]);

        
        //COMANDO RESPONSÁVEL POR POPULAR A VIEW MATERIALIZADA spoa_portal.vw_orgao_hierarquia
        DB::statement("REFRESH MATERIALIZED VIEW spoa_portal.vw_orgao_hierarquia;");
    }
}
