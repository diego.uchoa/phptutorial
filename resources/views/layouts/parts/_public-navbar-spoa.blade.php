<?php $helper = app('App\Helpers\UtilHelper'); ?>

<div id="navbar" class="navbar navbar-default navbar-collapse navbar-fixed-top">
	<div class="navbar-container" id="navbar-container">
		<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
			<span class="sr-only">Toggle sidebar</span>

			<span class="icon-bar"></span>

			<span class="icon-bar"></span>

			<span class="icon-bar"></span>
		</button>

		<div class="navbar-header pull-left">
			<!-- <a id="inicio_link" href="{{route('portal.inicio')}}" class="navbar-brand">
				<small>
					<img src="{{ URL::asset('assets/img/logo.png') }}" height="25">
					| @yield('nome_sistema')
				</small>
			</a> -->
			<a id="inicio_link" href="#" class="navbar-brand">
				<small>
					{{Html::image('icones/thumbnail_'.$helper->getSistema().'.png',$helper->getSistema(),array('height' => 25, 'style' => 'padding-right:10px'))}}
					 @yield('nome_sistema') 
				</small>
			</a>
		</div>
		


				

	</div><!-- /.navbar-container -->

</div>	
		