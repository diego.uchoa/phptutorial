 @foreach($items as $item)

  @if($item->hasChildren())
    @php ($activeChild = false)
    @foreach($item->children() as $child)
      @if(isActiveURL($child->url()))
        @php ($activeChild = true)
          @break
      @endif
    @endforeach

    @if($activeChild)
      <li class="open">
        <a class="dropdown-toggle" href="#">
    @else
      <li class="">
        <a class="dropdown-toggle" href="#">
    @endif
    
  
  @else
    <li class="{{ isActiveURL($item->url()) }}">
      <a href="{!! $item->url() !!}">
        
  @endif

  @if($item->icon)
  <i class="{!! $item->icon!!}"></i>
  @else
  <i class="menu-icon fa fa-caret-right"></i>
  @endif

  <span class="menu-text">{!! $item->title !!}</span>

  @if($item->hasChildren())
  <b class="arrow fa fa-angle-down"></b>
  @endif

</a>

<b class="arrow"></b>

@if($item->hasChildren())
<ul class="submenu">
  @include('layouts.parts._menu-spoa-items', array('items' => $item->children()))
</ul>     
@endif

</li>

@endforeach