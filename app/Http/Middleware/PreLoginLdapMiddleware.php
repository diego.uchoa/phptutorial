<?php

namespace App\Http\Middleware;

use Closure;
use MaskHelper;

class PreLoginLdapMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       
       $nr_cpf = MaskHelper::removeMascaraCpf($request['nr_cpf']);

       $resultDN = \Adldap::search()->where('uid', $nr_cpf)->select('dn')->get();

       if ( !$resultDN->isEmpty() ){

           list($account_prefix, $account_suffix) = explode($nr_cpf, $resultDN[0]->dn[0]);

           $provider = \Adldap::getDefaultProvider();
           $provider->getConfiguration()->set('account_prefix', $account_prefix);
           $provider->getConfiguration()->set('account_suffix', $account_suffix);

       }

       return $next($request);           
    }
}
