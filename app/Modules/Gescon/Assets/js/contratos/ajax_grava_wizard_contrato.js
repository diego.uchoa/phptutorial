/***********************************************************************************************
//FUNCIONALIDADES ESPECÍFICAS DO WIZARD
************************************************************************************************/
$('#fuelux-wizard-container')
    .ace_wizard()
    .on('actionclicked.fu.wizard' , function(e, info){
    	if (info.direction === 'previous') {
    		return;
        }else{
           	if (info.step == 6){
           		return;	
           	}else{
	           	var form = $("#formulario");
				$.fn.validaCampos(e, info, form);
           	}
        }
    })
    .on('finished.fu.wizard', function(e) {

    	//Função específicas para verificar se os campos da Aba Outras Informações foram informados mas não foram adicionados na lista
    	//       e enviar o formulário para alteração
    	$.fn.verificaPreenchimentoCamposOutrasInformacoes(e);

    });

//Função responsável por gravar todos os dados do Contrato
$.fn.enviarFormulario = function(){
	var url = document.getElementById("formulario").action;
    var form = new FormData();
	$.fn.desabilitarTodosCampos();

	//Recuperando os arquivos de upload, se houver
	var qtd_input_files = $('#formulario input[type="file"]').length;
	for (var i = 0; i < qtd_input_files; i++)
	{
	    var qtd_files = $('#formulario input[type="file"]')[i].files.length;   
	    var name = $('#formulario input[type="file"]')[i].name;
	    for (var j = 0; j < qtd_files; j++)
	    {
	        form.append(name, $('#formulario input[type="file"]')[i].files[j]);   
	    }
	}

    //Recuperando os dados do formulário
    var formData = $('#formulario').serializeArray();    
    jQuery.each( formData, function( i, field ) {
        form.append(field.name, field.value);   
    });
    
    $.ajax({
        url: url,
        type: 'POST',
        data: form,
        dataType: 'json',
        contentType: false, 
        processData: false,
        
        beforeSend: function() {
            dialogCreate = bootbox.dialog({
                title: '<i class="ace-icon fa fa-exchange"></i> Enviando',
                message: '<p class="text-center"><i class="fa-spin ace-icon fa fa-cog fa-spin fa-2x fa-fw blue"></i>Aguarde...</p>',
                closeButton: true
            });
        },
        success: function(data) {
            dialogCreate.init(function(){
                if (data.status == "success"){
                    dialogCreate.find('.modal-title').html('<i class="ace-icon fa fa-thumbs-o-up"></i> Resultado:');
                    dialogCreate.find('.bootbox-body').html('<p class="text-center"><i class="ace-icon fa fa-check fa-2x fa-fw green"></i>'+ data.msg +'</p>');
                    
                    setTimeout(function(){
                    	dialogCreate.modal('hide');
                    }, 3000);
                    
                    window.location.href = data.redirect_url;
                }else{
                    dialogCreate.find('.modal-title').html('<i class="ace-icon fa fa-bullhorn"></i> Alerta:');
                    var aviso = '<p class="text-left"><i class="ace-icon fa fa-exclamation fa-2x fa-fw red"></i>'+ data.msg +'</p>';
                    if (typeof data.detail != "undefined"){
                        aviso = aviso + '<ul class="list-unstyled spaced">';    
                        aviso = aviso + '<li>Erro:'+ data.detail + '</li>';
                        aviso = aviso + '</ul>';        
                    }
                    dialogCreate.find('.bootbox-body').html(aviso);
                }
            });    

        },
        error: function(data){
            if (typeof data.responseJSON == "undefined"){
                var erro = '<ul class="list-unstyled spaced">';    
                erro = erro + '<li><i class="ace-icon fa fa-exclamation-triangle red"></i>'+ data.statusText + '</li>';
                erro = erro + '</ul>';    
            }else{
                var result = $.parseJSON(data.responseJSON.detail);
                var erro = '<ul class="list-unstyled spaced">';
                $.each(result, function(i, field){
                    erro = erro + '<li><i class="ace-icon fa fa-exclamation-triangle red"></i>'+ field[0] + '</li>';
                });
                erro = erro + '</ul>';    
            }

            dialogCreate.init(function(){
                dialogCreate.find('.modal-title').html('<i class="ace-icon fa fa-bullhorn"></i> Alerta:');
                dialogCreate.find('.bootbox-body').html('<p class="text-center">'+ erro +'</p>');
            });    
            
        }
    })
}



//Função responsável por realizar a validação dos campos do WIZARD
$.fn.validaCampos = function(e, info, form){
	$.fn.validacaoCamposStepCorrente(e, info, form);
	if (!form.valid()){
		e.preventDefault();	
		$('#alert-step').show()
	}else{
		$('#alert-step').hide();
		$.fn.enviarFormularioPorEtapa();
		$.fn.validacaoCamposStepProxima(info, form);
	}
}

//Função responsável por ignorar a validação de campos do STEP CORRENTE
$.fn.validacaoCamposStepCorrente = function(e, info, formulario) {
	var step_atual = info.step;
	var steps = formulario.find(".step-pane");
	for (i = step_atual; i < steps.length; i++){
		fields = formulario.find('#'+steps[i].id).find(":input");
		fields.each(function( index ) {
			var id_campo = fields[index].id;
			if (id_campo.indexOf('[]') == -1){
				$('#'+fields[index].id).addClass("ignore");	
			}
		});
	}
	
	//Função específicas para verificar se os campos da listagem foram preenchidos mas não incluídos na lista
	$.fn.verificaPreenchimentoCamposListaByStep(e, formulario, step_atual);		   		

	//Função específica para campos tratados por um botão diferente do 'Próximo'
	$.fn.ignorarValidacaoEspecificaCamposByStep(formulario, step_atual);
}

//Função responsável por validade os campos do próximo STEP
$.fn.validacaoCamposStepProxima = function(info, formulario) {
	var step_proximo = info.step + 1;
	fields = formulario.find('#step-'+step_proximo).find(":input");
	fields.each(function( index ) {
		var id_campo = fields[index].id;
		if (id_campo.indexOf('[]') == -1){
			$('#'+fields[index].id).removeClass("ignore");
		}
	});
}

$.fn.desabilitarTodosCampos = function() {
	$('#formulario input,select,textarea').each(function(){
		$(this).prop("disabled", false);
	});
}

$.fn.enviarFormularioPorEtapa = function(){
	var site = window.location.protocol + "//" + window.location.host;
	var url = site + "/gescon/contratos/store_rascunho/";     

	var form = new FormData();

	//Recuperando os arquivos de upload, se houver
	var qtd_input_files = $('#formulario input[type="file"]').length;
	for (var i = 0; i < qtd_input_files; i++)
	{
	    var qtd_files = $('#formulario input[type="file"]')[i].files.length;   
	    var name = $('#formulario input[type="file"]')[i].name;
	    for (var j = 0; j < qtd_files; j++)
	    {
	        //form.append(name, $('#formulario input[type="file"]')[i].files[j]);   
	    }
	}

	//Recuperando os dados do formulário
    var formData = $('#formulario').serializeArray();    
    jQuery.each( formData, function( i, field ) {
        form.append(field.name, field.value);   
    });

    $.ajax({
        url: url,
        type: 'POST',
        data: form,
        dataType: 'json',
        contentType: false, 
        processData: false,
        
        success: function(data) {

            if (data.status == "success"){

            	$('#id_contrato').val(data.dados.id_contrato);
            	$('#id_contratada').val(data.dados.id_contratada);
            	$('#in_status_contrato').val(data.dados.in_status_contrato);

            }else{
            	var aviso = '<p class="text-left"><i class="ace-icon fa fa-exclamation fa-2x fa-fw red"></i>'+ data.msg +'</p>';
            	if (typeof data.detail != "undefined"){
            	    aviso = aviso + '<ul class="list-unstyled spaced">';    
            	    aviso = aviso + '<li>Erro:'+ data.detail + '</li>';
            	    aviso = aviso + '</ul>';        
            	}
            	dialogCreate = bootbox.dialog({
            	    title: '<i class="ace-icon fa fa-bullhorn"></i> Alerta:',
            	    message: aviso,
            	    closeButton: true
            	});
            }
        },
        error: function(data){
            if (typeof data.responseJSON == "undefined"){
                var erro = '<ul class="list-unstyled spaced">';    
                erro = erro + '<li><i class="ace-icon fa fa-exclamation-triangle red"></i>'+ data.statusText + '</li>';
                erro = erro + '</ul>';    
            }else{
                var result = $.parseJSON(data.responseJSON.detail);
                var erro = '<ul class="list-unstyled spaced">';
                $.each(result, function(i, field){
                    erro = erro + '<li><i class="ace-icon fa fa-exclamation-triangle red"></i>'+ field[0] + '</li>';
                });
                erro = erro + '</ul>';    
            }
            dialogCreate = bootbox.dialog({
                title: '<i class="ace-icon fa fa-bullhorn"></i> Alerta:',
                message: '<p class="text-center">'+ erro +'</p>',
                closeButton: true
            });
        }
    })
}