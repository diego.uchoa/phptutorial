//Método responsável por mostrar a Mensagem anterior ao envio do Ajax
$.fn.enviaMensagemAguardeAjax = function(){
	
	return bootbox.dialog({
        title: '<i class="ace-icon fa fa-exchange"></i> Enviando',
        message: '<p class="text-center"><i class="fa-spin ace-icon fa fa-cog fa-spin fa-2x fa-fw blue"></i>Aguarde...</p>',
        closeButton: true
    });

}

//Método responsável por mostrar a Mensagem de Erro Ajax
$.fn.enviaMensagemErroAjax = function(data){
	
	if (typeof data.responseJSON == "undefined"){
	    var erro = '<ul class="list-unstyled spaced">';    
	    erro = erro + '<li><i class="ace-icon fa fa-exclamation-triangle red"></i>'+ data.statusText + '</li>';
	    erro = erro + '</ul>';    
	}else{
	    var result = $.parseJSON(data.responseJSON.detail);
	    var erro = '<ul class="list-unstyled spaced">';
	    $.each(result, function(i, field){
	        erro = erro + '<li><i class="ace-icon fa fa-exclamation-triangle red"></i>'+ field[0] + '</li>';
	    });
	    erro = erro + '</ul>';    
	}
	dialogCreate = bootbox.dialog({
	    title: '<i class="ace-icon fa fa-bullhorn"></i> Alerta:',
	    message: '<p class="text-center">'+ erro +'</p>',
	    closeButton: true
	});

}

//Método responsável por mostrar a Mensagem de Sucesso Ajax
$.fn.enviaMensagemSucessoAjax = function(data,dialogAlerta){
	
	dialogAlerta.find('.modal-title').html('<i class="ace-icon fa fa-thumbs-o-up"></i> Resultado:');
    dialogAlerta.find('.bootbox-body').html('<p class="text-center"><i class="ace-icon fa fa-check fa-2x fa-fw green"></i>'+ data.msg +'</p>');
    
    setTimeout(function(){
        dialogAlerta.modal('hide');
    }, 3000);

}

//Método responsável por mostrar a Mensagem de Alerta Ajax
$.fn.enviaMensagemAlertaAjax = function(data,dialogAlerta){
	
	dialogAlerta.find('.modal-title').html('<i class="ace-icon fa fa-bullhorn"></i> Alerta:');
	var aviso = '<p class="text-left"><i class="ace-icon fa fa-exclamation fa-2x fa-fw red"></i>'+ data.msg +'</p>';
	if (typeof data.detail != "undefined"){
	    aviso = aviso + '<ul class="list-unstyled spaced">';    
	    aviso = aviso + '<li>Erro:'+ data.detail + '</li>';
	    aviso = aviso + '</ul>';    
	}
	dialogAlerta.find('.bootbox-body').html(aviso);

}