<?php
namespace App\Modules\Gescon\Services;

use Illuminate\Support\Collection;
use App\Modules\Gescon\Repositories\ContratoItemContratacaoTerceirizacaoRepository;
use DB;
use Exception;

class ContratoTerceirizacaoService extends ContratoService
{
	private $contratoItemContratacaoTerceirizacaoRepository;
	
	public function __construct(ContratoItemContratacaoTerceirizacaoRepository $contratoItemContratacaoTerceirizacaoRepository)
    {
        $this->contratoItemContratacaoTerceirizacaoRepository = $contratoItemContratacaoTerceirizacaoRepository;
        parent::__construct();
    }

	/**
	 * Insere Itens de Contratação do Contrato
	 *
	 * @param  Request $request
	 *
	 */
	public function store_item_contratacao($request)
	{
	 	try{

			$item_contratacao = $this->contratoItemContratacaoTerceirizacaoRepository->create($this->__preparaDadosItemContratacaoContrato($request));
	 		return $item_contratacao;

	 	}catch (Exception $e){

	 		throw new Exception($e->getMessage());

	 	}
	}

	/**
	 * Excluir Itens de Contratação do Contrato
	 *
	 * @param  int $id_contrato_item_contratacao_terceirizacao
	 *
	 * @return Response
	 */
	public function destroy_item_contratacao($id_contrato_item_contratacao_terceirizacao)
	{
	    $this->contratoItemContratacaoTerceirizacaoRepository->find($id_contrato_item_contratacao_terceirizacao)->delete();
	}

	/**
	 * Método responsável por preparar os dados recebidos dos Itens de Contratação que serão associados ao Contrato
	 *
	 */
	private function __preparaDadosItemContratacaoContrato($dados)
	{
		$array_item_contratacao_contrato = [];

		$array_item_contratacao_contrato['id_contrato'] = $dados['id_contrato'];
		$array_item_contratacao_contrato['id_orgao'] = $dados['id_unidade_atendida'];
		$array_item_contratacao_contrato['id_edificio'] = $dados['id_edificio_atendido'];
		$array_item_contratacao_contrato['id_tipo_item_contratacao'] = $dados['id_tipo_item'];
		$array_item_contratacao_contrato['qt_item_contratacao'] = $dados['qt_item_contratacao'];
		$array_item_contratacao_contrato['id_unidade_medida_item_contratacao'] = $dados['id_unidade_medida'];
		$array_item_contratacao_contrato['vl_item_contratacao'] = $dados['vl_item_contratacao'];
		
		return $array_item_contratacao_contrato;
	}

}