<?php

namespace App\Modules\Gescon\Repositories;

use App\Modules\Gescon\Models\Fiscal;
use App\Repositories\AbstractRepository;

class FiscalRepository extends AbstractRepository
{
    public function __construct(Fiscal $model)
    {
        $this->model = $model;
    }

    /**
    * Recuperar todos os registros ativos dos fiscais, ordenando-os pelo nome
    * @return Array Fiscal
    */
    public function findAllOrderByName()
    {
        $fiscais = $this->findAll(['no_fiscal']);
        return $fiscais;
    }	

    /**
    * Recuperar todos os dados do fiscal pelo CPF
    * @return Array Fiscal
    */
    public function findByCPF($cpf)
    {   
        $fiscal = $this->filterByAttribute('nr_cpf', $cpf);
        if (count($fiscal) > 0){
            $fiscal[0]['st_cadastrado'] = 1; //REGISTRO ATIVO
            return $fiscal[0];
        }else{
            $fiscal = $this->findDeleted([['nr_cpf', '=', $cpf]]);
            if (count($fiscal) > 0){
                $fiscal[0]['st_cadastrado'] = 2; //REGISTRO EXCLUÍDO
                return $fiscal[0];
            }else{
                return "";
            }   
        }
    }    

    /**
    * Restaurar todos os dados do fiscal pelo CPF
    * @return Array Fiscal
    */
    public function restoreByCPF($cpf)
    {   
        $fiscal = $this->findDeleted([['nr_cpf', '=', $cpf]]);
        if (count($fiscal) > 0){
            $this->restoreDeleted([['nr_cpf', '=', $cpf]]);
            return $fiscal[0];
        }else{
            return "";
        }   
    }        
}
