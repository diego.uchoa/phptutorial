<?php

class GesconHelper
{

	static function maskMoney($number) { 
	    if ($number != 0){
	    	$valor = explode('.', $number);
	    	if(count($valor) > 1){
	    		$novo_valor = $valor[0] . '.' . substr($valor[1], 0, 2);	
	    	}else{
	    		$novo_valor = $valor[0] . '.00';	
	    	}
	    	$novo_valor = money_format('%.2n', $novo_valor);
	    	return number_format($novo_valor, 2, ',', '.');
	    }else{
	    	return number_format($number, 2, ',', '.');
	    }
	} 
		
}