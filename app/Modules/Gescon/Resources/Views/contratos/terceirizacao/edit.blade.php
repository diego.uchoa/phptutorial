@extends('gescon::layouts.master')

@section('breadcrumbs-page')

    {!! Breadcrumbs::render('gescon::contratantes.index') !!}

@endsection

@section('content')
	
		
		<div class="alert alert-warning">
			<button class="close" type="button" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>
			<strong>Informação.</strong>
			<br>
			<br>
			<strong>
				<i class="ace-icon fa fa-info-circle"></i>
			</strong>
			Os dados do contrato serão salvos automaticamente a cada mudança de etapa, <strong>exceto os anexos</strong>, que serão salvos ao final do processo de atualização.
			<br>
		</div>
		
		<div class="widget-box">
			<div class="widget-header widget-header-blue widget-header-flat">
				<h4 class="widget-title lighter">Etapas do Contrato de {{ $ds_objeto }}</h4>
			</div>

			<div class="widget-body">
				<div class="widget-main">
					<div id="fuelux-wizard-container">
						    <ul class="steps">
					        <li data-step="1" class="active">
					            <span class="step">1</span>
					            <span class="title">Dados Iniciais</span>
					        </li>
					        <li data-step="2">
					            <span class="step">2</span>
					            <span class="title">Contratada</span>
					        </li>
					        <li data-step="3">
					            <span class="step">3</span>
					            <span class="title">Contratação</span>
					        </li>
					        <li data-step="4">
					            <span class="step">4</span>
					            <span class="title">Datas/Pagamentos/Variação</span>
					        </li>

					        <li data-step="5">
					            <span class="step">5</span>
					            <span class="title">Garantia/Fiscais</span>
					        </li>

					        <li data-step="6">
					            <span class="step">6</span>
					            <span class="title">Outras Informações</span>
					        </li>
					    </ul>
					    <hr />
					    <div class="step-content pos-rel">

					    	{!! Form::model($contrato, ['route'=>['gescon::contratos.update'], 'method'=>'put', 'id'=>'formulario']) !!}
		                    	
		                    	{!! Form::hidden('in_objeto', $inObjeto, ['class' => 'form-control', 'id' => 'in_objeto']) !!}
		                    	{!! Form::hidden('in_status_contrato', null, ['class' => 'form-control', 'id' => 'in_status_contrato']) !!}
		                    	<div class="alert alert-danger" id="alert-step" style="display:none">
		        	            	<strong>
		        	            		<i class="ace-icon fa fa-times"></i>
		        	            		Atenção!
		        	            	</strong>
		        	            		Os campos em vermelho são de preenchimento obrigatório.
		        	            	<br/>
		                    	</div>

					    		<div class="step-pane active" data-step="1" id="step-1">
						            <h3 class="lighter block blue">Dados Iniciais do Contrato</h3>
										@include('gescon::contratos._form_contratante')
						        </div>
						    
						        <div class="step-pane" data-step="2" id="step-2">
						            <h3 class="lighter block blue">Dados da Contratada</h3>
						            	@include('gescon::contratos._form_contratada')
						        </div>

						        <div class="step-pane" data-step="3" id="step-3">
						            <h3 class="lighter block blue">Dados da Contratação</h3>
						            	@include('gescon::contratos.terceirizacao._form_contratacao')
						        </div>

								<div class="step-pane" data-step="4" id="step-4">
								    <h3 class="lighter block blue">Dados das Datas, Processo de Pagamento e Variação</h3>
								    	@include('gescon::contratos._form_data_pagamento_variacao')
								</div>							        

								<div class="step-pane" data-step="5" id="step-5">
								    <h3 class="lighter block blue">Dados de Garantia e Fiscais</h3>
								    	@include('gescon::contratos._form_fiscal')
								</div>							        

								<div class="step-pane" data-step="6" id="step-6">
								    <h3 class="lighter block blue">Outras Informações</h3>
								    	@include('gescon::contratos._form_informacoes_adicionais')
								</div>							        

						    {!! Form::close() !!}

					    </div>
					</div>
					<hr />
					<div class="wizard-actions">
						<button class="btn btn-prev">
					        <i class="ace-icon fa fa-arrow-left"></i>Anterior
					    </button>
					    <button class="btn btn-success btn-next" data-last="Finalizar">
					        Próximo<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
					    </button>
					</div>
				</div>
			</div>
		</div>

@endsection

@section('script-end')
    
    @parent
	
	<!-- page specific plugin scripts -->
	<script src="{{ URL::asset('assets/js/jquery.maskedinput.min.js') }}"></script>
	<script src="{{ URL::asset('assets/js/wizard.min.js') }}"></script>
	<script src="{{ URL::asset('assets/js/jquery.validate.min.js') }}"></script>
	<script src="{{ URL::asset('assets/js/jquery.maskTelefone.min.js') }}"></script>
	<script src="{{ URL::asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
	<script src="{{ URL::asset('assets/js/jquery.maskMoney.min.js') }}"></script>
	<script src="{{ URL::asset('assets/js/jquery.inputlimiter.1.3.1.min.js') }}"></script>
	<script src="{{ URL::asset('modules/gescon/js/mask_contratada.js') }}"></script>
	<script src="{{ URL::asset('modules/gescon/js/ajax_mensagem.js') }}"></script>
	<script src="{{ URL::asset('modules/gescon/js/ajax_busca_cep.js') }}"></script>
	<script src="{{ URL::asset('modules/gescon/js/ajax_lista_municipios.js') }}"></script>
	<script src="{{ URL::asset('modules/gescon/js/ajax_busca_contratada.js') }}"></script>
	<script src="{{ URL::asset('modules/gescon/js/ajax_busca_dados_fiscal.js') }}"></script>
	<script src="{{ URL::asset('modules/gescon/js/contratos/ajax_busca_contrato.js') }}"></script>
	<script src="{{ URL::asset('modules/gescon/js/contratos/validate_campos_contrato_terceirizacao.js') }}"></script>
	<script src="{{ URL::asset('modules/gescon/js/contratos/validate_wizard_contrato_terceirizacao.js') }}"></script>
	<script src="{{ URL::asset('modules/gescon/js/contratos/ajax_grava_wizard_contrato.js') }}"></script>
	<script src="{{ URL::asset('js/select.js') }}"></script>

    <script type="text/javascript">
        jQuery(function($) {                
        	
        	/***********************************************************************************************
    		Métodos responsáveis pela funcionalidade de Data (Calendário)
    		************************************************************************************************/
			$.fn.data_picker = function() {
			    $('.date-picker').datepicker({
			        autoclose: true,
			        todayHighlight: true
			  
			    });

			    $('.data_limite').datepicker('setEndDate', new Date().toDateString())
			        .on('show', function(){
			            $('td.day.disabled').each(function(index, element){
			                var $element = $(element)
			                $element.attr("title", "A data não pode ser superior à atual");
			                $element.data("container", "body");
			                $element.tooltip()
			            });
			        });

			    $('.data_futura').datepicker('setStartDate', new Date().toDateString())
			        .on('show', function(){
			            $('td.day.disabled').each(function(index, element){
			                var $element = $(element)
			                $element.attr("title", "A data não pode ser inferior à atual");
			                $element.data("container", "body");
			                $element.tooltip()
			            });
			        });
			};

		    /***********************************************************************************************
			CARREGAMENTO DAS FUNCIONALIDADES/MASCARAS
			************************************************************************************************/
			$(document).ready(function() {
			    $('.input-mask-cpf').mask('999.999.999-99');
			    $('.input-mask-cep').mask('99.999-999');
			    $(".input-mask-money").maskMoney({showSymbol:true, symbol:"R$", decimal:",", thousands:"."});
			    $('.input-mask-numero-portaria').mask('999/9999');
			    $('.input-mask-numero-boletim').mask('999/9999');
			    $('.input-mask-numero-modalidade').mask('9999/9999');
			    $('.input-mask-numero-cronograma').mask('9999/9999');
			    $('.input-mask-numero-contrato').mask('9999/9999');
			    $('.input-mask-numero-processo').mask('99999.999999/9999-99');
			    $('.input-mask-numero-nota_empenho').mask('999999');
			    $('.input-mask-numero-elemento_despesa').mask('999999');
			    $('.input-mask-numero-matricula-siape').mask('9999999');
			    $('.input-mask-ano').mask('9999');
			    $.fn.mascaraTelefone();
			    $.fn.change_Campo_CPF_CNPJ();
			    $.fn.data_picker();
			    $.fn.file_campos();
			    $.fn.excluir_arquivo_modalidade();
			    $.fn.excluir_arquivo_contrato();
			});
	    });

	</script>

@endsection