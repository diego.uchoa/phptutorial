@extends('gescon::layouts.master')

@section('breadcrumbs-page')

    {!! Breadcrumbs::render('gescon::administracao.aviso_usuario.index') !!}

@endsection

@section('content')
    
    @section('page-header')
        Aviso Usuário
    @endsection
    
    <a href="#" class="btn btn-sm btn-primary insert" data-url="{{route('gescon::administracao.aviso_usuario.create')}}">
        <i class="ace-icon fa glyphicon-plus bigger-110"></i>
        Novo
    </a>

    <br>
    <br>

    <div class="table-container">
        @include('gescon::administracao.aviso_usuario._tabela')
    </div> 

    <div class="formulario-container">
        @include('gescon::administracao.aviso_usuario._modal')
    </div>     

@endsection

@section('script-end')

    @parent

    <script src="http://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="{{ URL::asset('assets/js/jquery.dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootbox.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function($) {                
            $.fn.dinamic_table = function() {
                var oTable = $('#dynamic-table').DataTable({
                    language: {
                      url: "{!! asset('modules/sismed/Portuguese-Brasil.json') !!}", //Arquivo tradução para português
                    },
                    processing: true,
                    serverSide: true,
                    ajax: '{!! route('gescon::administracao.aviso_usuario.records') !!}',
                    columns: [//Configura os campos da datatable com os campos enviados pelo Controller. Cada linha a baixo é uma linha da table do html, tem que ter a mesma quantidade de <th>
                        { data: 'tx_aviso_usuario', name: 'tx_aviso_usuario', width: '30%' },
                        { data: 'no_tipo_aviso_usuario', name: 'no_tipo_aviso_usuario', width: '12%' },
                        { data: 'nr_ordem', name: 'nr_ordem', width: '7%' },
                        { data: 'no_sistema', name: 'no_sistema', width: '10%' },
                        { data: 'no_usuario', name: 'no_usuario', width: '15%' },
                        { data: 'sn_lido', name: 'sn_lido', width: '8%' },
                        { data: 'dt_lido', name: 'dt_lido', width: '8%' },
                        { data: 'operacoes', name: 'operacoes', width: '10%' }
                    ],
                });
            };

            $(document).ready(function() {
                $.fn.dinamic_table(); 
            }); 

            $.fn.carregarFuncoes = function() {
                $.fn.chosen_select();
            };
        });
    </script>

    <script src="{{ asset('modules/sisadm/js/ajax_crud.js') }}"></script>

@endsection