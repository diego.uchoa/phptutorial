<?php

namespace App\Modules\Sisadm\Http\Controllers;

use App\Modules\Sisadm\Http\Requests\AvisoUsuarioRequest;

use App\Modules\Sisadm\Repositories\AvisoUsuarioRepository;
use App\Modules\Sisadm\Repositories\TipoAvisoUsuarioRepository;
use App\Modules\Sisadm\Repositories\SistemaRepository;
use App\Modules\Sisadm\Repositories\UserRepository;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Cache;
use App\Helpers\UtilHelper;


class AvisoUsuarioController extends Controller
{
    
    protected $repository;
    protected $tipoAvisoUsuarioRepository;
    protected $sistemaRepository;
    protected $usuarioRepository;


    public function __construct(AvisoUsuarioRepository $repository, TipoAvisoUsuarioRepository $tipoAvisoUsuarioRepository,SistemaRepository $sistemaRepository, UserRepository $usuarioRepository)
    {
        $this->repository = $repository;
        $this->tipoAvisoUsuarioRepository = $tipoAvisoUsuarioRepository;
        $this->sistemaRepository = $sistemaRepository;
        $this->usuarioRepository = $usuarioRepository;
    }

    public function index()
    {
        $mode = "";
        $pefixoView = $this->__montaPrefixoView();

        return view($pefixoView.'aviso_usuario.index', compact('mode'));
    }

    public function records(Request $request)
    {   
        $pefixoView = $this->__montaPrefixoView();
        $avisos = $this->repository->findAllOrderByName();

        return Datatables::of($avisos)
                
                ->addColumn('tx_aviso_usuario', function ($aviso) {
                                return $aviso->tx_aviso_usuario;
                            })
                ->addColumn('no_tipo_aviso_usuario', function ($aviso) {
                                return $aviso->tipo->no_tipo_aviso_usuario;
                            })
                ->addColumn('nr_ordem', function ($aviso) {
                                return $aviso->nr_ordem;
                            })
                ->addColumn('no_sistema', function ($aviso) {
                                return optional($aviso->sistema)->no_sistema;
                            })
                ->addColumn('no_usuario', function ($aviso) {
                            return optional($aviso->usuario)->no_usuario;
                        })  
                ->addColumn('sn_lido', function ($aviso) {
                            if ($aviso->sn_lido){
                                return 'Lido';
                            }
                            return ' - ';
                        })
                ->addColumn('dt_lido', function ($aviso) {
                            if ($aviso->dt_lido){
                                return $aviso->dt_lido;
                            }
                            return ' - ';
                        })

                ->addColumn('operacoes', function ($aviso) use ($pefixoView) {
                    return "<a href='#' data-url=".route($pefixoView.'aviso_usuario.edit', ['id'=>$aviso->id_aviso_usuario])." class='btn btn-xs btn-info update'><i class='ace-icon fa fa-pencil'></i></a>
                            <a href='#' data-id='{{ $aviso->id_aviso_usuario }}' data-url=".route($pefixoView.'aviso_usuario.destroy', ['id'=>$aviso->id_aviso_usuario])." class='btn btn-xs btn-danger delete'><i class='ace-icon fa fa-trash-o'></i></a>";
                            })
                ->rawColumns(['operacoes'])
                ->make(true);
    }

    public function create()
    {
        $mode = "create";
        $tipos = $this->tipoAvisoUsuarioRepository->lists('no_tipo_aviso_usuario','id_tipo_aviso_usuario');
        $sistemas = $this->sistemaRepository->listsByUsuario(UtilHelper::removeMascaraCpf(Auth::user()->nr_cpf));
        $usuarios = $this->usuarioRepository->lists('no_usuario','id_usuario');
        $pefixoView = $this->__montaPrefixoView();
        $html = view($pefixoView.'aviso_usuario._modal', compact('tipos','sistemas','usuarios', 'mode'))->render(); 

        return response(['msg' => '', 'status' => 'success', 'html'=> $html]);
    }

    public function store(AvisoUsuarioRequest $request)
    {
        $aviso = $this->repository->create($request->all());
        $idUsuario =  $aviso->id_usuario;
        $idSistema =  $aviso->id_sistema;

        //Alterar o cache
        $this->atualizaAvisoUsuarioCache($idUsuario,$idSistema);

        $html = $this->renderizarTabela();
        
        return response(['msg' => trans('alerts.registro.created'), 'status' => 'success', 'html'=> $html]);    
    }

    public function edit($id)
    {
        $mode = "update";
        $aviso = $this->repository->find($id);
        $tipos = $this->tipoAvisoUsuarioRepository->lists('no_tipo_aviso_usuario','id_tipo_aviso_usuario');
        $sistemas = $this->sistemaRepository->listsByUsuario(UtilHelper::removeMascaraCpf(Auth::user()->nr_cpf));
        $usuarios = $this->usuarioRepository->lists('no_usuario','id_usuario');
        $pefixoView = $this->__montaPrefixoView();
        $html = view($pefixoView.'aviso_usuario._modal', compact('aviso','tipos','sistemas','usuarios', 'mode'))->render(); 

        return response(['msg' => '', 'status' => 'success', 'html'=> $html]);    
    }

    public function update(AvisoUsuarioRequest $request, $id)
    {
        $avisoAntes = $this->repository->find($id);
        $this->repository->find($id)->update($request->all());

        $avisoDepois = $this->repository->find($id);

        $idUsuarioAntes =  $avisoDepois->id_usuario;
        $idSistemaAntes =  $avisoDepois->id_sistema;

        $idUsuarioDepois =  $avisoDepois->id_usuario;
        $idSistemaDepois =  $avisoDepois->id_sistema;

        //Alterar o cache
        $this->limpaAvisoSistemaCache($idUsuarioAntes,$idSistemaAntes);
        $this->atualizaAvisoUsuarioCache($idUsuarioDepois,$idSistemaDepois);

        $html = $this->renderizarTabela();

        return response(['msg' => trans('alerts.registro.updated'), 'status' => 'success', 'html'=> $html]);    
    }

    public function destroy($id)
    {
        try{

            $aviso = $this->repository->find($id);

            $idUsuario =  $aviso->id_usuario;
            $idSistema =  $aviso->id_sistema;

            $aviso->delete();

            //Alterar o cache
            $this->atualizaAvisoUsuarioCache($idUsuario,$idSistema);

            $html = $this->renderizarTabela();            
            
            return response(['msg' => trans('alerts.registro.deleted'), 'status' => 'success', 'html'=> $html]);

        }catch(Exception $e){

            return response(['msg' => trans('alerts.registro.deletedError'), 'detail' => $e->getMessage(), 'status' => 'error']);
            
        }     
    }

    private function limpaAvisoSistemaCache($idUsuario,$idSistema)
    {
        $usuario = $this->usuarioRepository->getCpfById($idUsuario);

        if($idSistema === NULL) {

            Cache::forget('menu-avisos-usuarios-geral-'.$usuario);
            Cache::forget('qtd-avisos-usuarios-geral-'.$usuario);

        }
        else {

            $sistema = $this->sistemaRepository->getNomeSistemaById($idSistema);

            Cache::forget('menu-avisos-usuarios-'.$sistema);                        
            Cache::forget('qtd-avisos-usuarios-'.$sistema);
        }
    }

    private function atualizaAvisoUsuarioCache($idUsuario,$idSistema)
    {
        
        $usuario = $this->usuarioRepository->getCpfById($idUsuario);

        if($idSistema === NULL) {

            $avisoGeral = $this->repository->geraAvisoUsuarioGeral($usuario);
            Cache::put('menu-avisos-usuarios-geral-'.$usuario, $avisoGeral, 60); 

            $qtdAvisoGeral = $this->repository->qtdAvisosNaoLidosGeral($usuario);
            Cache::put('qtd-avisos-usuarios-geral-'.$usuario, $qtdAvisoGeral,60);       
            
        }
        else {

           $sistema = $this->sistemaRepository->getNomeSistemaById($idSistema);

           $aviso = $this->repository->geraAvisoUsuario($usuario,$sistema);
           Cache::put('menu-avisos-usuarios-'.$sistema.'-'.$usuario, $aviso, 60);
           
           $qtdAviso = $this->repository->qtdAvisosNaoLidos($usuario,$sistema);
           Cache::put('qtd-avisos-usuarios-'.$sistema.'-'.$usuario, $qtdAviso,60);       
        }  
    }
    
    /**
     * Método responsável por renderizar a tabela da página de listagem
     * 
     * @return View
     */
    private function renderizarTabela()
    {
        //recuperando os Avisos do Usuario para renderizar a tabela
        $avisos = $this->repository->findAllOrderByName();                
        $pefixoView = $this->__montaPrefixoView();
        return view($pefixoView.'aviso_usuario._tabela', compact('avisos'))->render(); 
    }    

    /**
     * Método responsável por retornar uma View correta de acordo com o módulo que a chamou
     * @param string $module
     * @return string
    */
    private function __montaPrefixoView()
    {
        if (UtilHelper::getSistemaModulo(UtilHelper::getSistema()) == 'sisadm'){
            return 'sisadm::';
        }else{
            return UtilHelper::getSistemaModulo(UtilHelper::getSistema()).'::administracao.';
        }
    }
}