<table id="dynamic-table" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th class="center">
                <label class="pos-rel">
                    <input type="checkbox" class="ace" />
                    <span class="lbl"></span>
                </label>
            </th>
            <th>Perfil</th>
            <th>Sistema</th>
            <th>Itens de Menu</th>
        </tr>
    </thead>

    <tbody>
        
        @foreach($sistemas as $sistema)
                
                <tr>
                    <td style="background-color: #DDD"></td> 
                    <th style="background-color: #DDD" class="center">
                        {!! $sistema->no_sistema !!}
                    </th>
                    <td style="background-color: #DDD"></td>
                    <td style="background-color: #DDD"></td>
                </tr>
                
                @foreach($sistema->perfis as $perfil)
                <tr>
                    <td class="center">
                        <label class="pos-rel">
                            {!! Form::checkbox("perfis[]", $perfil->id_perfil, in_array($perfil->id_perfil,$perfilUsuario),['class'=>'ace']) !!}
                            <span class="lbl"></span>
                        </label>
                    </td>
                    <td>{!! $perfil->no_perfil !!} </td>
                    <td>{!! $perfil->sistema->no_sistema !!} </td>
                    <td>
                        @foreach($perfilService->getItensMenu($perfil->id_perfil) as $itemMenu)
                            <span class="label label-sm label-success arrowed"> {!! $itemMenu->no_item_menu !!} </span>
                        @endforeach
                    </td>
                </tr>
                @endforeach

        @endforeach
    </tbody>
</table>

<br>