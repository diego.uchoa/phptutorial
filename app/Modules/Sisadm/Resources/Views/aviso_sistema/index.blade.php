@extends('sisadm::layouts.master')

@section('breadcrumbs-page')

    {!! Breadcrumbs::render('sisadm::aviso_sistema.index') !!}

@endsection

@section('content')
    
    @section('page-header')
        Aviso Sistema
    @endsection
        
    <a href="#" class="btn btn-sm btn-primary insert" data-url="{{route('sisadm::aviso_sistema.create')}}">
        <i class="ace-icon fa glyphicon-plus bigger-110"></i>
        Novo
    </a>

    <br>
    <br>
    
    <div class="table-container">
        @include('sisadm::aviso_sistema._tabela')
    </div> 

    <div class="formulario-container">
        @include('sisadm::aviso_sistema._modal')
    </div>     

@endsection

@section('script-end')

    @parent

    <script src="http://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="{{ URL::asset('assets/js/jquery.dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootbox.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function($) {                
            $.fn.dinamic_table = function() {
                var oTable = $('#dynamic-table').DataTable({
                    language: {
                      url: "{!! asset('modules/sismed/Portuguese-Brasil.json') !!}", //Arquivo tradução para português
                    },
                    processing: true,
                    serverSide: true,
                    ajax: '{!! route('sisadm::aviso_sistema.records') !!}',
                    columns: [//Configura os campos da datatable com os campos enviados pelo Controller. Cada linha a baixo é uma linha da table do html, tem que ter a mesma quantidade de <th>
                        { data: 'tx_aviso_sistema', name: 'tx_aviso_sistema', width: '30%' },
                        { data: 'no_tipo_aviso_sistema', name: 'no_tipo_aviso_sistema', width: '20%' },
                        { data: 'nr_ordem', name: 'nr_ordem', width: '10%' },
                        { data: 'no_sistema', name: 'no_sistema', width: '20%' },
                        { data: 'sn_destaque', name: 'sn_destaque', width: '10%' },
                        { data: 'operacoes', name: 'operacoes', width: '10%' }
                    ],
                });
            };

            $(document).ready(function() {
                $.fn.dinamic_table(); 
            });

            $.fn.carregarFuncoes = function() {
                $.fn.chosen_select();
            };
        });
    </script>

    <script src="{{ URL::asset('modules/sisadm/js/ajax_crud.js') }}"></script>
    
@endsection