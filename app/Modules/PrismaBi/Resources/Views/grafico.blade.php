<!doctype html>
<html>
<head>

  <style>
    @page { margin-top: 20px; }
  
    body {
      background: rgb(255,255,255);
    }

    table {
      border-collapse: collapse;
      width: 100%;
      font-size: 80%;
    }
    table th {
      background-color: #FFF;
      color: #000;
      text-align: center;
    }
    th,
    td {
      border: 0px solid #FFF;
      text-align: left;
    }

    .logo {
      padding-top: 0px;
      text-align: center;
      border: 0px solid #ddd;
    }

    .imagem {
      height: 112px;
    }
    
    .imagem-icon {
      height: 8px;
      width:  8px;
    }

    .cabecalho {
      background-color: #ddd;
      text-align: center;
      font-weight: bold;
      font-family:Arial, sans-serif;
      font-size:16px;
    }

    .legenda {
      font-family:Arial, sans-serif;
      font-size:10px;
      margin-top: 40px;
    }

    .legenda-right {  
      float: right;
    }

    </style>

</head>

{!! Charts::assets() !!}

<body>
  
<div class="container">

  <div class="logo">
    <img src="{{ $path }}/modules/prismabi/imagens/spe-prisma-relatorio.png" class="imagem">
  </div>

  <div class="cabecalho">
    MINISTÉRIO DA FAZENDA
    <br>Secretaria de Política Econômica
    <br>{{ $mes }}
  </div>

  <div class="row">
       <div class="col-sm-5">
            {!! $grafico->render() !!}
        </div>
  </div>

</div>

</body>

</html>