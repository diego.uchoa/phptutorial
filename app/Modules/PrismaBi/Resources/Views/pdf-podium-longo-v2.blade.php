<!doctype html>
<html>
<head>
  
<style>
  @page { margin: 0px; }
  
  .imagem-cabecalho {
    height: 100%;
    width:  100%;
  }
  .container {
    margin-top: 10px;
    margin-left: 40px;
    margin-right: 40px;

    font-family:Helvetica,Arial, sans-serif;
  }

  .legenda-right {  
    display: inline;
    float: right;
    font-size:17px;
    font-weight: bold;
    color: #0e3455;
  }

  .titulo {  
    margin-top: 15px;
    margin-bottom: 15px;
    display: block;
    font-size:25px;
    font-weight: bold;
    color: #0e3455;
  }

  .tabela {
     border-collapse:collapse;
     border-spacing:0;
     border-color:#ffffff;
     width:100%;
  }

  .tb-variavel {
    padding: 4px 3px 4px 3px;
    font-size:17px;
    font-weight: bold;
    color: #ffffff;
    background: #0e3455;
  }

  .tb-espaco {
    padding:10px;
  }

  .tb-podium-col1 {
    font-size:11px;
    font-weight: normal;
    color: #414042;
    text-align:center;
    padding:3px 5px 3px 5px;
    width:  45px;
  }

  .tb-podium-img {
    text-align:center;
    border-right: 2px solid #ffffff;
    width:  5%;
  }

  .tb-podium-col3 {
    font-size:11px;
    font-weight: bold;
    color: #414042;
    text-align:left;
    padding:3px 5px 3px 5px;
    border-right: 2px solid #ffffff;
  }

  .tb-podium-col4 {
    font-size:11px;
    font-weight: normal;
    color: #414042;
    text-align:center;
    width:  150px;
  }

  .tb-podium-col3-demais {
    font-size:11px;
    font-weight: normal;
    color: #414042;
    text-align:right;
  }


  .tb-linha1 {
    background: #e6e7e8;
    border-top: 0;
  }
  .tb-linha2 {
    background: #f6f6f6;
  }

  .tb-podium-posicao {
    font-size:12px;
    font-weight: normal;
    color: #ffffff;
    background: #0f5e96;
    text-align:left;
    padding: 3px 5px 3px 5px;
  }

  .rodape {
    font-size:11px;
    font-weight: normal;
    color: #414042;
    text-align:justify;
    padding-top: 5px;
  }
</style>

</head>


<body>
  
  <div>
    <img src="{{ $path }}/modules/prismabi/imagens/cabecalho.jpg" class="imagem-cabecalho">
  </div>

  <div class="container">
     
    <div>
        <div class="legenda-right ">{{ $periodo }}</div> 
    </div>
    <br>
    <div class="titulo"> 
        Podium - 
        <label style="font-weight: normal;"> {{trans('prisma-bi::pdf.longo-prazo')}} </label> 
    </div>

   @foreach(json_decode($dados, true) as $value)

      <table class="tabela">
        <tr>
            <th class="tb-variavel" colspan="3" ><label style="padding-left:6px">
            {{ trans('prisma-bi::pdf.'. App\Modules\PrismaBi\Http\Controllers\PdfController::getNomeImg($value['variavel']) ) }}
            </label></th>
        </tr>
        <tr> 
          <td class="tb-podium-posicao">{{trans('prisma-bi::pdf.colocacao')}}</td>
          <td class="tb-podium-posicao">{{trans('prisma-bi::pdf.instituicao')}}</td>
          <td class="tb-podium-posicao" style="text-align:center">{{trans('prisma-bi::pdf.erro-absoluto-ponderado')}}</td>
        </tr>

        @foreach($value['instituicao'] as $key => $instituicao)
          <tr>
           @if (strcmp($instituicao,'demais instituições (média)')== 0 )
            <td class="tb-podium-col3-demais" colspan='2'>{{ trans('prisma-bi::pdf.demais-instituicoes')}}</td>
            <td class="tb-podium-col4">{{ $value['valor'][$key] }}</td>
           @else
            
              @if ($key % 2 == 0)
                <td class="tb-podium-col1 tb-linha1">{{$key + 1}}º </td>
                <td class="tb-podium-col3 tb-linha1">{{ $instituicao }}</td>
                <td class="tb-podium-col4 tb-linha1">{{ $value['valor'][$key] }}</td>
              @else
                 <td class="tb-podium-col1 tb-linha2">{{$key + 1}}º </td>
                 <td class="tb-podium-col3 tb-linha2">{{ $instituicao }}</td>
                 <td class="tb-podium-col4 tb-linha2">{{ $value['valor'][$key] }}</td>
              @endif

           @endif  
           </tr> 
        @endforeach  

     
        @if( strcmp($value['variavel'],'Dívida Bruta do Governo Geral')!= 0)
        <tr>
          <th class="tb-espaco" colspan="3"> </th>
        </tr>
        @endif

     </table>
    @endforeach

    <div class="rodape"> 
       {{ trans('prisma-bi::pdf.observacao2')}}
    </div>


</div>

</body>

</html>