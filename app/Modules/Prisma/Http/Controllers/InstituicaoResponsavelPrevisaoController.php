<?php

namespace App\Modules\Prisma\Http\Controllers;

use App\Modules\Prisma\Http\Requests\InstituicaoResponsavelPrevisaoRequest;
use App\Modules\Prisma\Repositories\InstituicaoResponsavelPrevisaoRepository;
use App\Http\Controllers\Controller;

class InstituicaoResponsavelPrevisaoController extends Controller
{
    /** @var  InstituicaoResponsavelPrevisaoRepository */
    private $instituicaoResponsavelPrevisaoRepository;

    public function __construct(InstituicaoResponsavelPrevisaoRepository $instituicaoResponsavelPrevisaoRepository) {
        $this->instituicaoResponsavelPrevisaoRepository = $instituicaoResponsavelPrevisaoRepository;
    }


    /**
     * Mostre o formulário para criar um novo InstituicaoResponsavelPrevisao.
     *
     */
    public function create() {   
        $mode = "create";

        $html = view('prisma::instituicoes_responsavel_previsao._modal', compact('mode'))->render(); 

        return response(['msg' => '', 'status' => 'success', 'html'=> $html]);
    }

    /**
     * Insere um InstituicaoResponsavelPrevisao recentemente criado.
     *
     * @param InstituicaoResponsavelPrevisaoRequest $request
     *
     */
    public function store(InstituicaoResponsavelPrevisaoRequest $request) {

        $id_responsavel = $this->instituicaoResponsavelPrevisaoRepository->create($request->all())->id_instituicao_responsavel_previsao;

        $instituicaoResponsavelPrevisao = $this->instituicaoResponsavelPrevisaoRepository->listaTodosSemVinculo();

        $html = view('prisma::instituicoes_responsavel_previsao._select', compact('instituicaoResponsavelPrevisao','id_responsavel'))->render(); 
        
        return response(['msg' => trans('alerts.registro.created'), 'status' => 'success', 'html'=> $html]);    

    }
}
