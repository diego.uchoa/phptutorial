<?php

namespace App\Modules\Prisma\Http\Controllers;

use App\Modules\Prisma\Repositories\InstituicaoRepository;
use App\Modules\Prisma\Services\ControleUsuarioService;

use App\Modules\Sisadm\Repositories\UserRepository;
use App\Modules\Sisadm\Repositories\PerfilRepository;
use App\Modules\Sisadm\Repositories\SistemaRepository;
use App\Modules\Sisadm\Services\UserService;
use App\Modules\Sisadm\Http\Requests\UsuarioRequest;
use App\Modules\Prisma\Http\Requests\ResponsavelRequest;
use App\Modules\Prisma\Http\Requests\EditorRequest;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Collection;
use Auth;
use App\Helpers\UtilHelper;
use Maatwebsite\Excel\Facades\Excel;


use App\Modules\Sisadm\Repositories\OrgaoRepository;

class ControleUsuarioController extends Controller
{

    protected $repository;
    protected $instituicaoRepository;
    protected $perfilRepository;
    protected $sistemaRepository;
    protected $controleUsuarioService;

    protected $userService;
    protected $orgaoRepository;
    
    public function __construct(ControleUsuarioService $controleUsuarioService,
        UserService $userService, UserRepository $repository, 
                                    InstituicaoRepository $instituicaoRepository, PerfilRepository $perfilRepository, SistemaRepository $sistemaRepository,
                                    OrgaoRepository $orgaoRepository)
    {
        $this->controleUsuarioService = $controleUsuarioService;
        $this->instituicaoRepository = $instituicaoRepository;
        $this->repository = $repository;
        $this->perfilRepository = $perfilRepository;
        $this->sistemaRepository = $sistemaRepository;

        $this->userService = $userService;
        $this->orgaoRepository = $orgaoRepository;
     }

    public function index() {
        return view('prisma::usuarios.index');
    }

    public function findUsuarioByCPF($cpf) {
        $usuario = $this->repository->findBy([['nr_cpf','=',$cpf]]);
        $retorno = array();

        if((!$usuario->isEmpty()) && ($usuario->has('instituicaoPrisma'))) {
            $retorno['status'] = 'EXISTENTE';
        }
        else {
            $retorno['status'] = 'SUCCESS';
        }

        return $retorno;
    }

    public function records(Request $request) {
        $usuarios = $this->repository->all()->where('instituicaoPrisma','!=',null);

        return Datatables::of($usuarios)
            ->addColumn('id_instituicao', function ($usuario) {
                return optional($usuario->instituicaoPrisma->instituicaoPrevisao)->no_instituicao_responsavel_previsao;
            })
            ->addColumn('instituicao', function ($usuario) {
                return '<a href="'.route('prisma::instituicoes.show',['id'=>$usuario->instituicaoPrisma->id_instituicao]).'">'.$usuario->instituicaoPrisma->no_razao_social.'</a>';
            })
            ->addColumn('nr_cpf', function ($usuario) {
                return $usuario->nr_cpf;
            })
            ->addColumn('no_usuario', function ($usuario) {
                return $usuario->no_usuario;
            })
            ->addColumn('email', function ($usuario) {
                return $usuario->email;
            })
            ->addColumn('telefone', function ($usuario) {
                return $usuario->instituicaoPrisma->pivot->nr_telefone;
            })
            ->addColumn('in_perfil_prisma', function ($usuario) {
                return $usuario->instituicaoPrisma->in_perfil_prisma;
            })
            ->rawColumns(['instituicao'])
            ->make(true);        
    }

    public function exportar() {
        $xlsFilename = date("YmdHis").'_usuarios_prisma';
        $usuarios = $this->repository->all()->where('instituicaoPrisma','!=',null)->sortBy('instituicaoPrisma.instituicaoPrevisao.no_instituicao_responsavel_previsao');

        $dadosXls = array(['ID da instituição','Instituição','CPF','Nome','E-mail','Telefone','Perfil']);
        foreach ($usuarios as $usuario) {
            array_push($dadosXls, [optional($usuario->instituicaoPrisma->instituicaoPrevisao)->no_instituicao_responsavel_previsao, $usuario->instituicaoPrisma->no_razao_social, $usuario->nr_cpf, $usuario->no_usuario, $usuario->email, $usuario->instituicaoPrisma->pivot->nr_telefone, $usuario->instituicaoPrisma->in_perfil_prisma]);
        }

        return Excel::create($xlsFilename, function($excel) use($dadosXls) {
            $excel->sheet('Usuários', function($sheet) use($dadosXls) {
                $sheet->cells('A1:G1', function($cells) {
                    $cells->setBackground('#dddddd');
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->fromArray($dadosXls,null,'A1',true,false);
            });
        })->download('xls');
    }

    public function createEditor($id_instituicao) {
        $mode = "create";
        $html = view('prisma::usuarios.editores._modal', compact('mode','id_instituicao'))->render(); 
        return response(['msg' => '', 'status' => 'success', 'html'=> $html]);
    }

    public function createResponsavel($id_instituicao) {
        $mode = "create";
        $html = view('prisma::usuarios.responsavel._modal', compact('mode','id_instituicao'))->render(); 
        return response(['msg' => '', 'status' => 'success', 'html'=> $html]);
    }

    public function storeEditor(EditorRequest $request) { 
        $usuario = $this->controleUsuarioService->store($request);

        $request['in_perfil'] = 'E';
        $request['no_cargo'] = null;

        if($usuario->getOriginalContent()['status'] == 'success'){

            $usuarioLogado = UtilHelper::getUsuario();

            if($usuarioLogado->hasPerfil('PRISMA-Gestor')){
                $instituicao = $this->instituicaoRepository->find($request['id_instituicao']);    
            }else {
                $instituicao = $usuarioLogado->instituicaoPrisma;
            }
            
            $perfilEditor = $this->controleUsuarioService->associarPerfil($request->nr_cpf,'PRISMA-EditorInstituicao');
            $usuarioAssociacao = $this->controleUsuarioService->associarUsuarioInstituicao($instituicao,$request);

            $html = view('prisma::instituicoes.show._dados_editores', compact('instituicao'))->render(); 

            return response(['msg' => $usuario->getOriginalContent()['msg'], 'status' => 'success', 'html'=> $html]); 

        }    

        return $usuario;     
        
    }

    public function storeResponsavel(ResponsavelRequest $request) { 
        $usuario = $this->controleUsuarioService->store($request);

        $request['in_perfil'] = 'R';

        if($usuario->getOriginalContent()['status'] == 'success'){

            $usuarioLogado = UtilHelper::getUsuario();

            if($usuarioLogado->hasPerfil('PRISMA-Gestor')){
                $instituicao = $this->instituicaoRepository->find($request['id_instituicao']);    
            }else {
                $instituicao = $usuarioLogado->instituicaoPrisma;
            }
            
            $perfilResponsavel = $this->controleUsuarioService->associarPerfil($request->nr_cpf,'PRISMA-ResponsavelInstituicao');
            $usuarioAssociacao = $this->controleUsuarioService->associarUsuarioInstituicao($instituicao,$request);

            $html = view('prisma::instituicoes.show._dados_responsavel', compact('instituicao'))->render(); 

            return response(['msg' => $usuario->getOriginalContent()['msg'], 'status' => 'success', 'html'=> $html]); 

        }    

        return $usuario;     
        
    }

    public function editEditor($id)
    {
        $mode = "update";
        $usuario = $this->repository->find($id);

        if($usuario->instituicaoPrisma) {
            $usuario->nr_telefone = $usuario->instituicaoPrisma->pivot->nr_telefone;  
            $id_instituicao = $usuario->instituicaoPrisma;  
        }
        
        $html = view('prisma::usuarios.editores._modal', compact('usuario','mode','id_instituicao'))->render(); 
        return response(['msg' => '', 'status' => 'success', 'html'=> $html]);

    }

    public function updateEditor(EditorRequest $request, $id)
    {
        $usuario = $this->repository->find($id);
        $request['id_instituicao'] = $usuario->instituicaoPrisma->id_instituicao;
        $usuario = $this->controleUsuarioService->update($request, $id);

        if($usuario->getOriginalContent()['status'] == 'success'){

            $usuarioLogado = UtilHelper::getUsuario();

            if($usuarioLogado->hasPerfil('PRISMA-Gestor')){
                $instituicao = $this->instituicaoRepository->find($request['id_instituicao']);    
            }else {
                $instituicao = $usuarioLogado->instituicaoPrisma;
            }

            $usuarioAssociacao = $this->controleUsuarioService->associarUsuarioInstituicao($instituicao,$request);

            $html = view('prisma::instituicoes.show._dados_editores', compact('instituicao'))->render(); 

            return response(['msg' => $usuario->getOriginalContent()['msg'], 'status' => 'success', 'html'=> $html]); 

        }

        return $usuario;
    }

    public function editResponsavel($id)
    {
        $mode = "update";
        $usuario = $this->repository->find($id);

        if($usuario->instituicaoPrisma) {
            $usuario->nr_telefone = $usuario->instituicaoPrisma->pivot->nr_telefone;    
            $usuario->no_cargo = $usuario->instituicaoPrisma->pivot->no_cargo;    
        }
        
        $html = view('prisma::usuarios.responsavel._modal', compact('usuario','mode'))->render(); 
        return response(['msg' => '', 'status' => 'success', 'html'=> $html]);

    }

    public function updateResponsavel(ResponsavelRequest $request, $id)
    {
        $usuario = $this->repository->find($id);
        $request['id_instituicao'] = $usuario->instituicaoPrisma->id_instituicao;
        $usuario = $this->controleUsuarioService->update($request, $id);

        if($usuario->getOriginalContent()['status'] == 'success'){

            $usuarioLogado = UtilHelper::getUsuario();

            if($usuarioLogado->hasPerfil('PRISMA-Gestor')){
                $instituicao = $this->instituicaoRepository->find($request['id_instituicao']);    
            }else {
                $instituicao = $usuarioLogado->instituicaoPrisma;
            }

            $usuarioAssociacao = $this->controleUsuarioService->associarUsuarioInstituicao($instituicao,$request);

            $html = view('prisma::instituicoes.show._dados_responsavel', compact('instituicao'))->render(); 

            return response(['msg' => $usuario->getOriginalContent()['msg'], 'status' => 'success', 'html'=> $html]); 

        }

        return $usuario;
    }

    public function changeResponsavel($id) {
        $mode = "change";
        $usuario = $this->repository->find($id);
        $id_instituicao = $usuario->instituicaoPrisma->id_instituicao;

        $html = view('prisma::usuarios.responsavel._modal', compact('mode','id_instituicao'))->render(); 
        return response(['msg' => '', 'status' => 'success', 'html'=> $html]);
    }

    public function alterResponsavel(ResponsavelRequest $request) {
        $usuarioLogado = UtilHelper::getUsuario();

        if($usuarioLogado->hasPerfil('PRISMA-Gestor')) {
            $instituicao = $this->instituicaoRepository->find($request['id_instituicao']);    
        }
        else {
            $instituicao = $usuarioLogado->instituicaoPrisma;
        }
        $responsavelAtual = $instituicao->responsavel->last();

        $instituicao->responsavel()->detach();
        $responsavelAtual->delete();
        
        $usuario = $this->controleUsuarioService->store($request);

        $request['in_perfil'] = 'R';

        if($usuario->getOriginalContent()['status'] == 'success') {      
            if($usuarioLogado->hasPerfil('PRISMA-Gestor')) {
                $instituicao = $this->instituicaoRepository->find($request['id_instituicao']);    
            }
            else {
                $instituicao = $usuarioLogado->instituicaoPrisma;
            }

            $perfilResponsavel = $this->controleUsuarioService->associarPerfil($request->nr_cpf,'PRISMA-ResponsavelInstituicao');
            $usuarioAssociacao = $this->controleUsuarioService->associarUsuarioInstituicao($instituicao,$request);

            if($usuarioLogado->hasPerfil('PRISMA-ResponsavelInstituicao')) {
                return response(['msg' => $usuario->getOriginalContent()['msg'], 'status' => 'logout', 'html'=> '']); 
            }

            $html = view('prisma::instituicoes.show._dados_responsavel', compact('instituicao'))->render(); 

            return response(['msg' => $usuario->getOriginalContent()['msg'], 'status' => 'success', 'html'=> $html]); 

        }    

        return $usuario;     
    }

    public function destroyEditor($id) {
        try {

            $usuario = $this->repository->find($id);
            $instituicaoPrisma = $usuario->instituicaoPrisma;
            $usuario->instituicaoPrisma()->detach();
            $usuario->delete();
            $usuarioLogado = UtilHelper::getUsuario();

            if($usuarioLogado->hasPerfil('PRISMA-Gestor')) {
                $instituicao = $this->instituicaoRepository->find($instituicaoPrisma->id_instituicao);    
            }
            else {
                $instituicao = $usuarioLogado->instituicaoPrisma;
            }

            $html = view('prisma::instituicoes.show._dados_editores', compact('instituicao'))->render(); 

            return response(['msg' => 'Registro excluído com sucesso.', 'status' => 'success', 'html'=> $html]);

        } catch(Exception $e) {
            return response(['msg' => 'Erro ao excluir o registro.', 'status' => 'error']);
        }
    }

    public function destroyResponsavel($id) {
        try {

            $usuario = $this->repository->find($id);
            $instituicaoPrisma = $usuario->instituicaoPrisma;
            $usuario->instituicaoPrisma()->detach();
            $usuario->delete();

            $usuarioLogado = UtilHelper::getUsuario();

            if($usuarioLogado->hasPerfil('PRISMA-Gestor')) {
                $instituicao = $this->instituicaoRepository->find($instituicaoPrisma->id_instituicao);    
            }
            else {
                $instituicao = $usuarioLogado->instituicaoPrisma;
            }

            if($usuarioLogado->hasPerfil('PRISMA-ResponsavelInstituicao')) {
                return response(['msg' => 'Registro excluído com sucesso.', 'status' => 'logout', 'html'=> '']); 
            }

            $html = view('prisma::instituicoes.show._dados_responsavel', compact('instituicao'))->render(); 

            return response(['msg' => 'Registro excluído com sucesso.', 'status' => 'success', 'html'=> $html]);

        } catch(Exception $e) {
            \Log::info($e);
            return response(['msg' => 'Erro ao excluir o registro.', 'status' => 'error']);
        }
    }

}
