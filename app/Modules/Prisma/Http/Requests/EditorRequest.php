<?php

namespace App\Modules\Prisma\Http\Requests;

use App\Http\Requests\Request;

class EditorRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nr_cpf'   =>  'required|cpf',
            'no_usuario'   =>  'required',
            'email'   =>  'required',
            'nr_telefone'   =>  'required',
        ];
    }

    /**
     * Tratando as mensagens de valiação do formulário
     *
     * @return array
    */
    public function messages()
    {
        return [
            'nr_cpf.required' => 'O campo "CPF" é obrigatório.',
            'nr_cpf.cpf' => 'O CPF informado não é válido',
            'no_usuario.required' => 'O campo "Nome" é obrigatório.',
            'email.required' => 'O campo "E-mail" é obrigatório.',
            'nr_telefone.required' => 'O campo "Telefone" é obrigatório.',
        ];
    }
}
