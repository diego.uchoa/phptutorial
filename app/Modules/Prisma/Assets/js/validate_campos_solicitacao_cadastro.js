$('#formulario').validate({
	errorElement: 'div',
	focusInvalid: false,
	ignore: ".ignore",
	errorClass: 'help-block',
	rules: {
		nr_cnpj: {
			required: true
		},
		no_relatorio: {
			required: true
		},
		nr_cpf_responsavel: {
			required: true
		},
		no_responsavel: {
			required: true
		},
		nr_telefone_responsavel: {
			required: true
		},
		ds_email_responsavel: {
			required: true,
			email: true
		},
		no_cargo_responsavel: {
			required: true
		},

		nr_cpf_editor: {
			required: true
		},
		no_editor: {
			required: true
		},
		nr_telefone_editor: {
			required: true
		},
		ds_email_editor: {
			required: true,
			email: true
		},
	},
	messages: {
		nr_cnpj : {
			required: " ",
		},
		no_relatorio: {
			required: "",
		},
		nr_cpf_responsavel: {
			required: ""
		},
		no_responsavel: {
			required: ""
		},
		nr_telefone_responsavel: {
			required: ""
		},
		ds_email_responsavel: {
			required: "",
			email: ""
		},
		no_cargo_responsavel: {
			required: ""
		},

		nr_cpf_editor: {
			required: ""
		},
		no_editor: {
			required: ""
		},
		nr_telefone_editor: {
			required: ""
		},
		ds_email_editor: {
			required: "",
			email: ""
		},
	},
	
	highlight: function (e) {
		$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
	},
	unhighlight: function(e) {
		$(e).closest('.form-group').removeClass('has-error');
	},
	success: function (e) {
		$(e).closest('.form-group').removeClass('has-error');
		$(e).remove();
	},
	errorPlacement: function (error, element) {
		if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
			var controls = element.closest('div[class*="col-"]');
			if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
			else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
		}
		else if(element.is('.select2')) {
			error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
		}
		else if(element.is('.chosen-select')) {
			error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
		}
		else if(element.is('.date-picker')) {
			error.insertAfter(element.siblings('[class*="date-picker"]:eq(0)'));
		}
		else error.insertAfter(element.parent());
	},
	submitHandler: function (form) {
	},
	invalidHandler: function (form) {
	}
});