<div class="modal fade" id="modal-create" role="dialog" data-toggle="modal">
	
	<div class="modal-dialog">

		<div class="modal-content">
					{!! Form::model($instituicao, ['route'=>['prisma::instituicoes.update.instituicao_responsavel_previsao', $instituicao->id_instituicao], 'method'=>'put', 'id'=>'formulario']) !!}
					{!! Form::hidden('id_instituicao', $instituicao->id_instituicao) !!}
					<div class="modal-header">
						<a class="close" data-dismiss="modal">&times;</a>
						<h3>
							Editar ID da instituição
							<small>
						        <i class="ace-icon fa fa-angle-double-right"></i>
						        {{$instituicao->no_razao_social}}
						    </small>	
						</h3>
					</div>	

					<div class="modal-body">
						<h5 class="smaller">Selecione um ID previamente cadastrado e disponível ou adicione um novo.</h5>
						<div id="select-responsavel">
							@include('prisma::instituicoes_responsavel_previsao._select')
						</div>
						@permission('prisma::instituicoes_responsavel_previsao.store')
							<a href="#" class="insert-responsavel" data-url="{{route('prisma::instituicoes_responsavel_previsao.create')}}"><i class="ace-icon fa fa-plus bigger-110"></i> Adicionar novo ID</a>
						@endpermission
					</div>
		        	
		        	<div class="modal-footer">
						{!! Form::button('Salvar', ['class'=>'btn btn-sm btn-primary', 'id' => 'btnFormSalvarAJAX_prisma']) !!}
					</div>

		        {!! Form::close() !!}

        	<div id='snippet'>
			   
			</div>

			<!-- In case that we want redraw section-->
			<div id='mySection'>
			    
			</div>
			
		</div>

	</div>

</div>