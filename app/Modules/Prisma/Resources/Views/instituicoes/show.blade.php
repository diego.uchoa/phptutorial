@extends('prisma::layouts.master')

@section('breadcrumbs-page')
  
    @permission('prisma::instituicoes.show.minha')
        {!! Breadcrumbs::render('prisma::instituicoes.show.minha', $instituicao) !!}
    @endpermission

    @permission('prisma::instituicoes.show.todas')
        {!! Breadcrumbs::render('prisma::instituicoes.show', $instituicao) !!}
    @endpermission

@endsection


@section('page-header')
    Instituição 
    <small>
        <i class="ace-icon fa fa-angle-double-right"></i>
        {{ $instituicao->no_razao_social }}
    </small>
@endsection


@section('content')
	<div class="row">
	    <div class="col-xs-12 col-sm-5">
            <div id="dados-instituicao-responsavel-previsao">
                @include('prisma::instituicoes.show._dados_instituicao_responsavel_previsao')
            </div>
	    	<br>
	    	<div id="dados-instituicao">
                @include('prisma::instituicoes.show._dados_instituicao')
            </div>
        </div>

	    <div class="col-xs-12 col-sm-7">
	    	<div id="dados-responsavel">
	    	    @include('prisma::instituicoes.show._dados_responsavel')
	    	</div>
	    	<br>
	    	<div id="dados-editores">
	    	    @include('prisma::instituicoes.show._dados_editores')
	    	</div>
        </div>
    </div>  

    <div class="formulario-container"></div>  
    <div class="formulario-container-2"></div>  
@endsection


@section('script-end')
    
    @parent

    <script src="{{ URL::asset('assets/js/jquery.maskedinput.min.js') }}"></script>

    <script type="text/javascript">

        $(function() {

            $.fn.inputMaiusculo = function() {
                $('#no_relatorio').keyup(function() {
                    this.value = this.value.toUpperCase();
                });

                $('#no_instituicao_responsavel_previsao').keyup(function() {
                    this.value = this.value.toUpperCase();
                });
            };

            $.fn.carregarFuncoes = function() {
                $.fn.inputMaiusculo();
                $('.input-mask-cpf').mask('999.999.999-99');
                $('.input-mask-telefone').focusout(function(){
                    var phone, element;
                    element = $(this);
                    element.unmask();
                    phone = element.val().replace(/\D/g, '');
                    if(phone.length > 10) {
                        element.mask("(99) 99999-999?9");
                    } else {
                        element.mask("(99) 9999-9999?9");
                    }
                }).trigger('focusout');
            };

            $(document).ready(function() {
                $.fn.inputMaiusculo();
            });

            $(document).on('click','.insert-responsavel', function(event) {

                var url_create = $(this).data('url');

                dialogCreate = bootbox.dialog({
                    message: '<p class="text-center"><i class="fa-spin ace-icon fa fa-cog fa-spin fa-2x fa-fw blue"></i>Aguarde...</p>',
                    closeButton: false
                });

                $.get(url_create, function (data) {
                    dialogCreate.modal('hide');
                    $('.formulario-container-2').html(data.html);
                    $('#modal-create-2').modal('show');
                    
                    //Método responsável por adicionar o estilo ao select e a mascara, se necessário
                    $('#modal-create-2').on('shown.bs.modal', function () {
                        $.fn.carregarFuncoes();
                        $('.update').off();
                        $('.delete').off();
                        $('.insert').off();
                    });
                })

            });

            $(document).on('click','#btnFormSalvarAJAX_idResponsavel', function() {
                var url = document.getElementById("formulario-instituicao-responsavel-previsao").action;
                var form = new FormData();
            
                //Recuperando os dados do formulário
                var formData = $('#formulario-instituicao-responsavel-previsao').serializeArray();    
                jQuery.each( formData, function( i, field ) {
                    form.append(field.name, field.value);   
                });

                $('#btnFormSalvarAJAX_idResponsavel').prop("disabled",true);
                 
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: form,
                    dataType: 'json',
                    contentType: false, 
                    processData: false,

                    beforeSend: function() {
                        dialogCreate2 = bootbox.dialog({
                            title: '<i class="ace-icon fa fa-exchange"></i> Enviando',
                            message: '<p class="text-center"><i class="fa-spin ace-icon fa fa-cog fa-spin fa-2x fa-fw blue"></i>Aguarde...</p>',
                            closeButton: true
                        });
                    },
                    success: function(data) {
                        dialogCreate2.init(function(){
                            if (data.status == "success"){
                                dialogCreate2.find('.modal-title').html('<i class="ace-icon fa fa-thumbs-o-up"></i> Resultado:');
                                dialogCreate2.find('.bootbox-body').html('<p class="text-center"><i class="ace-icon fa fa-check fa-2x fa-fw green"></i>'+ data.msg +'</p>');

                                $('#select-responsavel').html(data.html);

                                setTimeout(function(){
                                    $('#modal-create-2').hide();
                                    dialogCreate2.modal('hide');
                                }, 500);
                             }
                             else {
                                dialogCreate2.find('.modal-title').html('<i class="ace-icon fa fa-bullhorn"></i> Alerta:');
                                var aviso = '<p class="text-left"><i class="ace-icon fa fa-exclamation fa-2x fa-fw red"></i>'+ data.msg +'</p>';
                                if (typeof data.detail != "undefined") {
                                    aviso = aviso + '<ul class="list-unstyled spaced">';    
                                    aviso = aviso + '<li>Erro:'+ data.detail + '</li>';
                                    aviso = aviso + '</ul>';        
                                }
                                dialogCreate2.find('.bootbox-body').html(aviso);
                            }
                        });    

                        $('#btnFormSalvarAJAX_idResponsavel').prop("disabled",false); 
                    },
                    error: function(data){
                        if (typeof data.responseJSON == "undefined"){
                            var erro = '<ul class="list-unstyled spaced">';    
                            erro = erro + '<li><i class="ace-icon fa fa-exclamation-triangle red"></i>'+ data.statusText + '</li>';
                            erro = erro + '</ul>';    
                        }else{
                            var result = $.parseJSON(data.responseJSON.detail);
                            var erro = '<ul class="list-unstyled spaced">';
                            $.each(result, function(i, field){
                                erro = erro + '<li><i class="ace-icon fa fa-exclamation-triangle red"></i>'+ field[0] + '</li>';
                            });
                            erro = erro + '</ul>';    
                        }

                        dialogCreate2.init(function(){
                            dialogCreate2.find('.modal-title').html('<i class="ace-icon fa fa-bullhorn"></i> Alerta:');
                            dialogCreate2.find('.bootbox-body').html('<p class="text-center">'+ erro +'</p>');
                        });    

                        $('#btnFormSalvarAJAX_idResponsavel').prop("disabled",false);
                    }
                }).done(function() {
                    $("#formulario-instituicao-responsavel-previsao")[0].reset();
                });

                return false;
            });
            
        });

        
    </script>

    <script src="{{ asset('modules/sisadm/js/ajax_crud.js') }}"></script>
    <script src="{{ asset('modules/prisma/js/instituicoes.js') }}"></script>

@endsection