@extends('prisma::layouts.master')

@section('breadcrumbs-page')
    {!! Breadcrumbs::render('prisma::solicitacao.cadastro.index') !!}

@endsection

@section('content')
        
    @section('page-header')
        Solicitações de Cadastro
    @endsection

<div class="search-area well well-sm" style="background: #EFF3F8; border: 0;">
    <div class="form-group">
        {!! Form::label('in_situacao', '<i class="ace-icon fa fa-search light-grey bigger-110"></i> <span class="blue">Filtrar solicitações pela situação:</span>',[],false) !!}
        {!! Form::select('in_situacao',['T' => 'Todas', 'P' => 'Pendentes', 'A' => 'Aprovadas', 'R' => 'Rejeitadas'],'P',['id' => 'select-situacao']) !!}
    </div>
</div>
    
    <br>

    <div class="table-container">
        @include('prisma::solicitacoes_cadastro.analise._tabela')
    </div> 

@endsection


@section('script-end')
    <script src="{{ URL::asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/jquery.dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootbox.min.js') }}"></script>

    <script type="text/javascript">
            jQuery(function($) {                
                $.fn.dinamic_table = function() {
                    var oTable = $('#dynamic-table').DataTable({
                        destroy: true,
                        language: {
                          url: "{!! asset('modules/sisadm/Portuguese-Brasil.json') !!}", //Arquivo tradução para português
                        },
                        processing: true,
                        serverSide: true,
                        ajax: '{!! route('prisma::solicitacao.cadastro.list') !!}'+'?filter=pendentes',
                        order: [],
                        columns: [//Configura os campos da datatable com os campos enviados pelo Controller. Cada linha a baixo é uma linha da table do html, tem que ter a mesma quantidade de <th>
                            { data: 'nr_cnpj', name: 'nr_cnpj', width: '15%' },
                            { data: 'no_razao_social', name: 'no_razao_social', width: '20%' },
                            { data: 'no_relatorio', name: 'no_relatorio', width: '20%' },
                            { data: 'no_responsavel', name: 'no_responsavel', width: '20%' },
                            { data: 'in_situacao', name: 'in_situacao', width: '15%' },
                            { data: 'operacoes', name: 'operacoes', width: '10%' }
                        ],
                        drawCallback: function(settings) {
                            $("[data-rel=tooltip]").tooltip();
                        }
                    });
                };

                $(document).on('click','#btn_all', function() {
                    var oTable = $('#dynamic-table').DataTable({
                        destroy: true,
                        language: {
                          url: "{!! asset('modules/sisadm/Portuguese-Brasil.json') !!}", //Arquivo tradução para português
                        },
                        processing: true,
                        serverSide: true,
                        ajax: '{!! route('prisma::solicitacao.cadastro.list') !!}'+'?filter=all',
                        order: [],
                        columns: [//Configura os campos da datatable com os campos enviados pelo Controller. Cada linha a baixo é uma linha da table do html, tem que ter a mesma quantidade de <th>
                            { data: 'nr_cnpj', name: 'nr_cnpj', width: '15%' },
                            { data: 'no_razao_social', name: 'no_razao_social', width: '20%' },
                            { data: 'no_relatorio', name: 'no_relatorio', width: '20%' },
                            { data: 'no_responsavel', name: 'no_responsavel', width: '20%' },
                            { data: 'in_situacao', name: 'in_situacao', width: '15%' },
                            { data: 'operacoes', name: 'operacoes', width: '10%' }
                        ],
                        drawCallback: function(settings) {
                            $("[data-rel=tooltip]").tooltip();
                        }
                    });
                });

                $('#select-situacao').change(function() {
                    var selected = $('#select-situacao').val();
                    
                    if(selected == 'T') {
                        var oTable = $('#dynamic-table').DataTable({
                            destroy: true,
                            language: {
                              url: "{!! asset('modules/sisadm/Portuguese-Brasil.json') !!}", //Arquivo tradução para português
                            },
                            processing: true,
                            serverSide: true,
                            ajax: '{!! route('prisma::solicitacao.cadastro.list') !!}'+'?filter=todas',
                            order: [],
                            columns: [//Configura os campos da datatable com os campos enviados pelo Controller. Cada linha a baixo é uma linha da table do html, tem que ter a mesma quantidade de <th>
                                { data: 'nr_cnpj', name: 'nr_cnpj', width: '15%' },
                                { data: 'no_razao_social', name: 'no_razao_social', width: '20%' },
                                { data: 'no_relatorio', name: 'no_relatorio', width: '20%' },
                                { data: 'no_responsavel', name: 'no_responsavel', width: '20%' },
                                { data: 'in_situacao', name: 'in_situacao', width: '15%' },
                                { data: 'operacoes', name: 'operacoes', width: '10%' }
                            ],
                            drawCallback: function(settings) {
                                $("[data-rel=tooltip]").tooltip();
                            }
                        });
                    }
                    else if(selected == 'A') {
                        var oTable = $('#dynamic-table').DataTable({
                            destroy: true,
                            language: {
                              url: "{!! asset('modules/sisadm/Portuguese-Brasil.json') !!}", //Arquivo tradução para português
                            },
                            processing: true,
                            serverSide: true,
                            ajax: '{!! route('prisma::solicitacao.cadastro.list') !!}'+'?filter=aprovadas',
                            order: [],
                            columns: [//Configura os campos da datatable com os campos enviados pelo Controller. Cada linha a baixo é uma linha da table do html, tem que ter a mesma quantidade de <th>
                                { data: 'nr_cnpj', name: 'nr_cnpj', width: '15%' },
                                { data: 'no_razao_social', name: 'no_razao_social', width: '20%' },
                                { data: 'no_relatorio', name: 'no_relatorio', width: '20%' },
                                { data: 'no_responsavel', name: 'no_responsavel', width: '20%' },
                                { data: 'in_situacao', name: 'in_situacao', width: '15%' },
                                { data: 'operacoes', name: 'operacoes', width: '10%' }
                            ],
                            drawCallback: function(settings) {
                                $("[data-rel=tooltip]").tooltip();
                            }
                        });
                    }
                    else if(selected == 'R') {
                        var oTable = $('#dynamic-table').DataTable({
                            destroy: true,
                            language: {
                              url: "{!! asset('modules/sisadm/Portuguese-Brasil.json') !!}", //Arquivo tradução para português
                            },
                            processing: true,
                            serverSide: true,
                            ajax: '{!! route('prisma::solicitacao.cadastro.list') !!}'+'?filter=rejeitadas',
                            order: [],
                            columns: [//Configura os campos da datatable com os campos enviados pelo Controller. Cada linha a baixo é uma linha da table do html, tem que ter a mesma quantidade de <th>
                                { data: 'nr_cnpj', name: 'nr_cnpj', width: '15%' },
                                { data: 'no_razao_social', name: 'no_razao_social', width: '20%' },
                                { data: 'no_relatorio', name: 'no_relatorio', width: '20%' },
                                { data: 'no_responsavel', name: 'no_responsavel', width: '20%' },
                                { data: 'in_situacao', name: 'in_situacao', width: '15%' },
                                { data: 'operacoes', name: 'operacoes', width: '10%' }
                            ],
                            drawCallback: function(settings) {
                                $("[data-rel=tooltip]").tooltip();
                            }
                        });
                    }
                    else {
                        $.fn.dinamic_table();
                    }
                });

                $(document).ready(function() {
                    $.fn.dinamic_table(); 
                });

            });

        </script>

    <script src="{{ asset('modules/sisadm/js/ajax_delete.js') }}"></script>
@endsection