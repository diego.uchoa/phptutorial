@extends('prisma::layouts.master')

@section('breadcrumbs-page')
  
  {!! Breadcrumbs::render('prisma::solicitacao.cadastro.edit', $solicitacaoCadastro) !!}

@endsection


@section('page-header')
  Solicitação de Cadastro 
  <small>
        <i class="ace-icon fa fa-angle-double-right"></i>
        {{ $solicitacaoCadastro->no_razao_social }}
  </small>
@endsection


@section('content')

@if($solicitacaoCadastro->in_situacao_solicitacao == 'R')
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        <p>
            <strong>
                <i class="ace-icon fa fa-times"></i>
                Solicitação rejeitada!
            </strong>
            A solicitação foi rejeitada em {{ $solicitacaoCadastro->dt_analise }} por {{ $solicitacaoCadastro->usuarioAnalise->no_usuario }}.
        </p>
    </div>
@elseif($solicitacaoCadastro->in_situacao_solicitacao == 'A')
    <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        <p>
            <strong>
            <i class="ace-icon fa fa-check"></i>
                Solicitação aprovada!
            </strong>
            A solicitação foi aprovada em {{ $solicitacaoCadastro->dt_analise }} por {{ $solicitacaoCadastro->usuarioAnalise->no_usuario }}.
        </p>
    </div>
@elseif($solicitacaoCadastro->in_situacao_solicitacao == 'E')
    <div class="alert alert-block alert-warning">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        <p>
            <strong>
            <i class="ace-icon fa fa-info-circle"></i>
                Solicitação em análise!
            </strong>
            A solicitação está sendo analisada, com última alteração em {{ $solicitacaoCadastro->updated_at }}.
        </p>
    </div>
@elseif($solicitacaoCadastro->in_situacao_solicitacao == 'P')
    <div class="alert alert-block alert-warning">
        <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
        <p>
            <strong>
            <i class="ace-icon fa fa-info-circle"></i>
                Solicitação aguardando análise!
            </strong>
        </p>
    </div>
@endif
	
@include('prisma::solicitacoes_cadastro.analise._dados_solicitacao')


@if($solicitacaoCadastro->in_situacao_solicitacao == 'P' || $solicitacaoCadastro->in_situacao_solicitacao == 'E')
    {!! Form::model($solicitacaoCadastro, ['route'=>['prisma::solicitacao.cadastro.update', $solicitacaoCadastro->id_solicitacao_cadastro], 'method'=>'put', 'class' => 'form-horizontal', 'id' => 'formulario']) !!}
        <div class="row">
            <div class="col-xs-12">
                <div class="search-area well well-sm">

                    <h5 class="navy">
                        <i class="ace-icon fa fa-link light-grey bigger-110"></i>
                        ID da instituição
                    </h5>

                    <div class="widget-main">
                        <div class="row">
                            <h5 class="smaller">Selecione um ID previamente cadastrado e disponível ou adicione um novo.</h5>
                            <div class="col-xs-12 col-sm-2" id="select-responsavel">
                                @include('prisma::instituicoes_responsavel_previsao._select')
                                @permission('prisma::instituicoes_responsavel_previsao.store')
                                    <a href="#" class="insert-responsavel" data-url="{{route('prisma::instituicoes_responsavel_previsao.create')}}"><i class="ace-icon fa fa-plus bigger-110"></i> Adicionar novo ID</a>
                                @endpermission
                            </div>
                        </div>
                    </div>   

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="search-area well well-sm">

                    <h5 class="navy">
                        <i class="ace-icon fa fa-check light-grey bigger-110"></i>
                        Análise de Solicitação de Cadastro
                    </h5>

                    <div class="widget-main">
                        @include('prisma::solicitacoes_cadastro.analise._form',['submit_text' => 'Salvar'])
                    </div>   

                </div>
            </div>
        </div>
    {!! Form::close() !!} 
@elseif($solicitacaoCadastro->in_situacao_solicitacao == 'A' || $solicitacaoCadastro->in_situacao_solicitacao == 'R')
    <div class="row">
        <div class="col-xs-12">
            <div class="search-area well well-sm">

                <h5 class="navy">
                    <i class="ace-icon fa fa-link light-grey bigger-110"></i>
                    ID da instituição
                </h5>

                <div class="widget-main">
                    {{ optional($solicitacaoCadastro->instituicaoPrevisao)->no_instituicao_responsavel_previsao }}
                </div>   

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="search-area well well-sm">

                <h5 class="navy">
                    <i class="ace-icon fa fa-check light-grey bigger-110"></i>
                    Análise de Solicitação de Cadastro
                </h5>

                <div class="widget-main">
                    {{ $solicitacaoCadastro->tx_analise }}
                </div>   

            </div>
        </div>
    </div>
@endif

<div class="formulario-container">
</div> 
  
@endsection

@section('script-end')

    <script type="text/javascript">
        $('#bt_aprovar').click(function() {
            var rota = "{!! route('prisma::solicitacao.cadastro.aprovar'); !!}";
            $('#formulario').attr('action', rota);
            $('#formulario').submit();
        });

        $('#bt_rejeitar').click(function() {
            var rota = "{!! route('prisma::solicitacao.cadastro.rejeitar'); !!}";
            $('#formulario').attr('action', rota);
            $('#formulario').submit();
        });

        $.fn.inputMaiusculo = function() {
            $('#no_instituicao_responsavel_previsao').keyup(function() {
                this.value = this.value.toUpperCase().trim();
            });
        };

        $(document).on('click','.insert-responsavel', function(event) {
            var url_create = $(this).data('url');

            dialogCreate = bootbox.dialog({
                message: '<p class="text-center"><i class="fa-spin ace-icon fa fa-cog fa-spin fa-2x fa-fw blue"></i>Aguarde...</p>',
                closeButton: false
            });

            $.get(url_create, function (data) {
                dialogCreate.modal('hide');
                $('.formulario-container').html(data.html);
                $('#modal-create-2').modal('show');
                
                //Método responsável por adicionar o estilo ao select e a mascara, se necessário
                $('#modal-create-2').on('shown.bs.modal', function () {
                    $.fn.carregarFuncoes();
                    $('.update').off();
                    $('.delete').off();
                    $('.insert').off();
                });
            })

        });

        $(document).on('click','#btnFormSalvarAJAX_idResponsavel', function() {
            var url = document.getElementById("formulario-instituicao-responsavel-previsao").action;
            var form = new FormData();
       
            //Recuperando os dados do formulário
            var formData = $('#formulario-instituicao-responsavel-previsao').serializeArray();    
            jQuery.each( formData, function( i, field ) {
                form.append(field.name, field.value);   
            });

            $('#btnFormSalvarAJAX_idResponsavel').prop("disabled",true);
             
            $.ajax({
                url: url,
                type: 'POST',
                data: form,
                dataType: 'json',
                contentType: false, 
                processData: false,

                beforeSend: function() {
                    dialogCreate = bootbox.dialog({
                        title: '<i class="ace-icon fa fa-exchange"></i> Enviando',
                        message: '<p class="text-center"><i class="fa-spin ace-icon fa fa-cog fa-spin fa-2x fa-fw blue"></i>Aguarde...</p>',
                        closeButton: true
                    });
                },
                success: function(data) {
                    dialogCreate.init(function(){
                        if (data.status == "success"){
                            dialogCreate.find('.modal-title').html('<i class="ace-icon fa fa-thumbs-o-up"></i> Resultado:');
                            dialogCreate.find('.bootbox-body').html('<p class="text-center"><i class="ace-icon fa fa-check fa-2x fa-fw green"></i>'+ data.msg +'</p>');

                            //Reload Tabela
                            $('#select-responsavel').html(data.html);

                            setTimeout(function(){
                                $('#modal-create-2').hide();
                                dialogCreate.modal('hide');
                            }, 3000);
                         }
                         else {
                            dialogCreate.find('.modal-title').html('<i class="ace-icon fa fa-bullhorn"></i> Alerta:');
                            var aviso = '<p class="text-left"><i class="ace-icon fa fa-exclamation fa-2x fa-fw red"></i>'+ data.msg +'</p>';
                            if (typeof data.detail != "undefined") {
                                aviso = aviso + '<ul class="list-unstyled spaced">';    
                                aviso = aviso + '<li>Erro:'+ data.detail + '</li>';
                                aviso = aviso + '</ul>';        
                            }
                            dialogCreate.find('.bootbox-body').html(aviso);
                        }
                    });    

                    $('#btnFormSalvarAJAX_idResponsavel').prop("disabled",false); 
                },
                error: function(data){
                    if (typeof data.responseJSON == "undefined"){
                        var erro = '<ul class="list-unstyled spaced">';    
                        erro = erro + '<li><i class="ace-icon fa fa-exclamation-triangle red"></i>'+ data.statusText + '</li>';
                        erro = erro + '</ul>';    
                    }else{
                        var result = $.parseJSON(data.responseJSON.detail);
                        var erro = '<ul class="list-unstyled spaced">';
                        $.each(result, function(i, field){
                            erro = erro + '<li><i class="ace-icon fa fa-exclamation-triangle red"></i>'+ field[0] + '</li>';
                        });
                        erro = erro + '</ul>';    
                    }

                    dialogCreate.init(function(){
                        dialogCreate.find('.modal-title').html('<i class="ace-icon fa fa-bullhorn"></i> Alerta:');
                        dialogCreate.find('.bootbox-body').html('<p class="text-center">'+ erro +'</p>');
                    });    

                    $('#btnFormSalvarAJAX_idResponsavel').prop("disabled",false);
                }
            }).done(function() {
                $("#formulario-instituicao-responsavel-previsao")[0].reset();
            });

            return false;
        });
        
        $.fn.carregarFuncoes = function() {
            $.fn.inputMaiusculo();
        };

    </script>

    <script src="{{ asset('modules/sisadm/js/ajax_crud.js') }}"></script>

    <style>
        .profile-instituicao-info {
            display: table;
            width: 98%;
            width: calc(100% - 24px);
            margin: 0 auto;
        }
        .profile-instituicao-name {
            text-align: right;
            padding: 6px 10px 6px 4px;
            font-weight: 400;
            color: #667E99;
            background-color: transparent;
            border-top: 1px dotted #D5E4F1;
            display: table-cell;
            width: 180px;
            vertical-align: middle;
        }
        .profile-instituicao-responsavel-name {
            text-align: right;
            padding: 6px 10px 6px 4px;
            font-weight: 400;
            color: #667E99;
            background-color: transparent;
            border-top: 1px dotted #D5E4F1;
            display: table-cell;
            width: 110px;
            vertical-align: middle;
        }
        .profile-instituicao-value {
            display: table-cell;
            padding: 6px 4px 6px 6px;
            border-top: 1px dotted #D5E4F1;
        }
        .profile-instituicao-value>span+span:before {
            display: inline;
            content: ", ";
            margin-left: 1px;
            margin-right: 3px;
            color: #666;
            border-bottom: 1px solid #FFF;
        }
        .profile-instituicao-value>span+span.editable-container:before {
            display: none;
        }

    </style>

@endsection