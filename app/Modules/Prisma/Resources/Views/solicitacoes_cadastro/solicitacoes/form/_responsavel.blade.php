<div class="row">
    <div class="col-lg-2">
        <div class="form-group required">
            {!! Form::label('nr_cpf_responsavel', 'CPF:') !!}
            {!! Form::text('nr_cpf_responsavel', null, ['class'=>'form-control input-mask-cpf', 'id' => 'nr_cpf_responsavel']) !!}
        </div>
    </div>
    <div class="col-lg-10">
        <div class="form-group required">
            {!! Form::label('no_responsavel', 'Nome:') !!}
            {!! Form::text('no_responsavel', null, ['class'=>'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-2">
        <div class="form-group required">
            {!! Form::label('nr_telefone_responsavel', 'Telefone:') !!}
            {!! Form::text('nr_telefone_responsavel', null, ['class'=>'form-control  input-mask-telefone']) !!}
        </div>
    </div>
    <div class="col-lg-5">
        <div class="form-group required">
            {!! Form::label('ds_email_responsavel', 'E-mail:') !!}
            {!! Form::email('ds_email_responsavel', null, ['id' => 'ds_email_responsavel', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-lg-5">
        <div class="form-group required">
            {!! Form::label('no_cargo_responsavel', 'Cargo:') !!}
            {!! Form::text('no_cargo_responsavel', null, ['class'=>'form-control']) !!}
        </div>
    </div>
</div>

@section('script-end')
    @parent

@endsection