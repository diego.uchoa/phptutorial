<p>
	Para solicitar participação no Prisma Fiscal, insira as informações solicitadas no formulário de solicitação de cadastro.
</p>
<p>
	Após o preenchimento, a equipe do Prisma Fiscal analisará os dados informados pela instituição e um e-mail será enviado ao responsável 
	cadastrado com o status da solicitação (aprovação ou rejeição).
</p>
<br>
<br>