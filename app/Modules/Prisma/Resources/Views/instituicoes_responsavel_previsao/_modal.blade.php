<div class="modal fade" id="modal-create-2" role="dialog" data-toggle="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			{!! Form::open(['route'=>'prisma::instituicoes_responsavel_previsao.store', 'id'=>'formulario-instituicao-responsavel-previsao']) !!}
				<div class="modal-header">
					<a class="close" data-dismiss="modal">&times;</a>
					<h3>Criar novo ID da instituição </h3>
				</div>	

				<div class="modal-body">
					<p>O ID da instituição deve ser único e não pode conter espaços em branco.</p>
					@include('prisma::instituicoes_responsavel_previsao._form',['submit_text' => 'Salvar'])
				</div>
	        	
	        	<div class="modal-footer">
					{!! Form::button('Salvar', ['class'=>'btn btn-sm btn-primary', 'id' => 'btnFormSalvarAJAX_idResponsavel']) !!}
				</div>

	        {!! Form::close() !!}

        	<div id='snippet'>
			   
			</div>

			<!-- In case that we want redraw section-->
			<div id='mySection'>
			    
			</div>
			
		</div>

	</div>

</div>